\babel@toc {spanish}{}
\contentsline {chapter}{\numberline {1}Preámbulo}{5}{chapter.1}%
\contentsline {section}{\numberline {1.1}Notas y Advertencias}{5}{section.1.1}%
\contentsline {section}{\numberline {1.2}Cuadros de código fuente}{6}{section.1.2}%
\contentsline {section}{\numberline {1.3}Introducción}{7}{section.1.3}%
\contentsline {chapter}{\numberline {2}Herramientas}{8}{chapter.2}%
\contentsline {section}{\numberline {2.1}Soldador}{8}{section.2.1}%
\contentsline {section}{\numberline {2.2}Estaño}{10}{section.2.2}%
\contentsline {section}{\numberline {2.3}Pinza}{11}{section.2.3}%
\contentsline {section}{\numberline {2.4}Destornillador}{12}{section.2.4}%
\contentsline {section}{\numberline {2.5}Desoldador de Estaño}{13}{section.2.5}%
\contentsline {chapter}{\numberline {3}Listado de Componentes}{14}{chapter.3}%
\contentsline {chapter}{\numberline {4}Armado de la placa \textbf {La.C.E.R.Ar V1.3 }}{18}{chapter.4}%
\contentsline {section}{\numberline {4.1}Resistencias de 470 ohms}{20}{section.4.1}%
\contentsline {section}{\numberline {4.2}Diodos y puente de alambre}{21}{section.4.2}%
\contentsline {section}{\numberline {4.3}LM7805}{22}{section.4.3}%
\contentsline {section}{\numberline {4.4}zócalo 8x2}{23}{section.4.4}%
\contentsline {section}{\numberline {4.5}capacitor 01. uF}{25}{section.4.5}%
\contentsline {section}{\numberline {4.6}LEDs}{26}{section.4.6}%
\contentsline {section}{\numberline {4.7}Pines Hembras}{28}{section.4.7}%
\contentsline {section}{\numberline {4.8}Borneras dobles}{29}{section.4.8}%
\contentsline {section}{\numberline {4.9}Pushbutton (Botón de reset)}{30}{section.4.9}%
\contentsline {section}{\numberline {4.10}Rele 5Vc}{31}{section.4.10}%
\contentsline {section}{\numberline {4.11}Capacitores electroliticos}{32}{section.4.11}%
\contentsline {section}{\numberline {4.12}Integrado ULN2003}{33}{section.4.12}%
\contentsline {section}{\numberline {4.13}Weemos D1 Mini}{34}{section.4.13}%
\contentsline {section}{\numberline {4.14}Pantalla OLED 128x32 pixeles}{35}{section.4.14}%
\contentsfinish 
