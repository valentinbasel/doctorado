\chapter{Preámbulo}
%\section{Convenciones del documento}

%\section{Convenciones tipográficas}

\section{Notas y Advertencias}
Utilizamos tres estilos visuales para llamar la atención sobre la información que de otro
modo se podría pasar por alto.

\begin{advertencia}
Las advertencias no deben ignorarse. Ignorarlas muy probablemente ocasionará pérdida de
datos.
\end{advertencia}

\begin{nota}
Una nota es una sugerencia, atajo o enfoque alternativo para una tarea determinada. Ignorar
una nota no debería tener consecuencias negativas, pero podría perderse de algunos trucos que
pueden facilitarle las cosas.
\end{nota}

\begin{importante}
Los cuadros con el título de importante dan detalles de cosas que se pueden pasar por alto
fácilmente: cambios de configuración únicamente aplicables a la sesión actual, o servicios
que necesitan reiniciarse antes de que se aplique una actualización. Ignorar estos cuadros no
ocasionará pérdida de datos, pero puede causar enfado y frustración.
\end{importante}

\section{Cuadros de código fuente.}

Para terminar, los cuadros que contienen código fuente se dividen entre código C y código escrito en lenguaje PYTHON.

Los cuadros de código fuente escrito en lenguaje ANSI C son de color violeta oscuro:
\begin{C}{Código fuente en lenguaje ANSI C.}
#include <stdio.h>
int main(void)
{
  printf("Hola, mundo");
} 
\end{C} 

Los cuadros de código fuente escrito en lenguaje PYTHON son de color azul:

\begin{python} {Código fuente en lenguaje Python}
# un comentario en python.
print "Hola Mundo"
\end{python}

En el caso de las llamadas a código escrito para la terminal \linux no se usaran cuadro de diálogos, pero las instrucciones serán tabuladas y presentadas como en el siguiente ejemplo.

\noindent el siguiente es un código de la terminal \linux:

\begin{lstlisting}[language=bash]
  $ sudo dnf install -y icaro
\end{lstlisting}
