#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from turtle import *
import random

global ej 
def grabar(ej):
    """TODO: Docstring for graba.
    :returns: TODO

    """
    ts = getscreen()
    ts.getcanvas().postscript(file="img/ej_"+str(ej)+".eps")
    #home()
    #clear()
    reset()
    colormode(255)
    speed(0)

ej=0
colormode(255)
speed(0)


###############################################################################
##    Empiezo los ejercicio
##    cuadrado
###############################################################################
ej = 1
for a in range(4):
    forward(100)
    left(90)
grabar(ej)

###############################################################################
##     silla
###############################################################################
ej = 2
left(90)
forward(40)
backward(20)
right(90)
forward(20)
right(90)
forward(20)
grabar(ej)

###############################################################################
ej = 3
left(90)
forward(40)
backward(20)
right(90)
forward(16)
right(90)
forward(20)
left(90)
penup()
forward(24)
left(90)
pendown()
grabar(ej)

###############################################################################
ej +=1

left(90)
forward(30)
right(90)
forward(60)
right(90)
forward(30)
left(90)
penup()
forward(24)
left(90)
pendown()
grabar(ej)

###############################################################################
ej +=1

left(90)
forward(60)
backward(15)
right(90)
forward(6)
left(90)
forward(15)
backward(15)
left(90)
forward(12)
right(90)
forward(15)
grabar(ej)

###############################################################################
ej +=1
right(90)
for a in range(4):
    forward(100)
    left(90)
left(90)
for a in range(3):
    forward(100)
    left(360/3)
grabar(ej)

###############################################################################
ej +=1

def cuadrado():
    for a in range(4):
        forward(100)
        left(90)

left(45)
cuadrado()
grabar(ej)


###############################################################################
##     silla
###############################################################################
ej +=1

def silla():
    left(90)
    forward(40)
    backward(20)
    right(90)
    forward(20)
    right(90)
    forward(20)
silla()

grabar(ej)

###############################################################################
##  cuatro
###############################################################################

ej += 1
left(180)
silla()
grabar(ej)

###############################################################################
##  seis
###############################################################################

ej += 1

def seis():
    silla()
    right(90)
    forward(20)
seis()
grabar(ej)

###############################################################################
##  nueve
###############################################################################

ej += 1
left(180)
seis()
grabar(ej)

exitonclick()
