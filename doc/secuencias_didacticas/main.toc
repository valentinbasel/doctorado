\babel@toc {spanish}{}
\contentsline {chapter}{\numberline {1}Preámbulo}{5}{chapter.1}%
\contentsline {section}{\numberline {1.1}Notas y Advertencias}{5}{section.1.1}%
\contentsline {section}{\numberline {1.2}Cuadros de código fuente}{6}{section.1.2}%
\contentsline {chapter}{\numberline {2}Introducción}{7}{chapter.2}%
\contentsline {section}{\numberline {2.1}domótica}{7}{section.2.1}%
\contentsline {chapter}{\numberline {3}Sobre la secuencia didáctica}{9}{chapter.3}%
\contentsline {section}{\numberline {3.1}TORCAZ}{9}{section.3.1}%
\contentsline {chapter}{\numberline {4}preparar nuestro entorno de trabajo}{11}{chapter.4}%
\contentsline {subsection}{\numberline {4.0.1}Crear Token para nuestro BOT}{11}{subsection.4.0.1}%
\contentsline {subsection}{\numberline {4.0.2}sobre ID y seguridad en TELEGRAM}{14}{subsection.4.0.2}%
\contentsline {chapter}{\numberline {5}BOT}{15}{chapter.5}%
\contentsline {subsection}{\numberline {5.0.1}Ejercicios}{15}{subsection.5.0.1}%
\contentsline {subsubsection}{Hola mundo}{15}{section*.2}%
\contentsline {subsubsection}{Ejercicio 0}{16}{section*.3}%
\contentsline {subsubsection}{Ejercicio 1}{16}{section*.4}%
\contentsline {subsubsection}{enviar imágenes}{16}{section*.5}%
\contentsline {subsubsection}{recibir texto}{17}{section*.6}%
\contentsline {subsection}{\numberline {5.0.2}actividades}{17}{subsection.5.0.2}%
\contentsline {chapter}{\numberline {6}domoticaro}{18}{chapter.6}%
\contentsline {section}{\numberline {6.1}WEMOS}{18}{section.6.1}%
\contentsline {section}{\numberline {6.2}Ejercicios}{18}{section.6.2}%
\contentsline {subsubsection}{conectar la placa a python}{18}{section*.7}%
\contentsline {subsubsection}{prender y apagar un rele}{19}{section*.8}%
\contentsline {subsubsection}{Leer el sensor de temperatura y humedad}{20}{section*.9}%
\contentsline {subsubsection}{leer un sensor analógico}{20}{section*.10}%
\contentsline {subsubsection}{funciones extras de los reles}{21}{section*.11}%
\contentsline {section}{\numberline {6.3}Actividades}{21}{section.6.3}%
\contentsline {chapter}{\numberline {7}proyectos integradores}{23}{chapter.7}%
\contentsfinish 
