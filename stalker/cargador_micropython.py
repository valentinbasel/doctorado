#!/usr/bin/env python3
# -*- coding: utf-8 -*-


###############################################################################
# programa para cargar la configuración de micro python
# Copyright © 2020 Valentin Basel <valentinbasel@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################

import gi
# import tarfile
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import os


class MENSAJE(Gtk.Dialog):
    def __init__(self, parent,msj):
        Gtk.Dialog.__init__(
            self,
            "mensaje",
            parent,
            0,
            (
                Gtk.STOCK_OK,
                Gtk.ResponseType.OK,
            ),
        )

        self.set_default_size(150, 100)

        label = Gtk.Label(msj)

        box = self.get_content_area()
        box.add(label)
        self.show_all()


class CARGADOR(object):

    """Docstring for CARGADOR. """

    def __init__(self):
        """TODO: to be defined. """
        cad = "Cargador de configuraciones para micropython"
        self.main_windows = Gtk.Window(title=cad)
        self.main_windows.set_default_size(-1, 350)
        self.main_windows.connect("destroy", self.close_main)

        Vbox = Gtk.VBox(spacing=6)

        Hbox1 = Gtk.HBox(spacing=6)
        lconf = Gtk.Label("SSDID")
        self.tconf = Gtk.Entry()
        Hbox1.pack_start(lconf, True, True, 16)
        Hbox1.pack_start(self.tconf, True, True, 0)
        Vbox.pack_start(Hbox1, True, True, 0)
 
        Hbox2 = Gtk.HBox(spacing=6)
        lpass = Gtk.Label("Contraseña")
        self.Tpass = Gtk.Entry()
        Hbox2.pack_start(lpass, True, True, 0)
        Hbox2.pack_start(self.Tpass, True, True, 0)
        Vbox.pack_start(Hbox2, True, True, 0)

        Hbox3 = Gtk.HBox(spacing=6)
        ltty = Gtk.Label("puerto")
        self.Ttty = Gtk.Entry(text="/dev/ttyUSB0")
        Hbox3.pack_start(ltty, True, True, 16)
        Hbox3.pack_start(self.Ttty, True, True, 0)
        Vbox.pack_start(Hbox3, True, True, 0)

        Hbox4 = Gtk.HBox(spacing=6)
        lbaud = Gtk.Label("puerto")
        self.Tbaud = Gtk.Entry(text="115200")
        Hbox4.pack_start(lbaud, True, True, 16)
        Hbox4.pack_start(self.Tbaud, True, True, 0)
        Vbox.pack_start(Hbox4, True, True, 0)
        
        Hbox5 = Gtk.HBox(spacing=6)
        Bcarg = Gtk.Button("cargar")
        Bcarg.connect("clicked", self.cargar)
        Bsalir = Gtk.Button("salir")
        Bsalir.connect("clicked", self.close_main)
        Hbox5.pack_start(Bcarg, True, True, 0)
        Hbox5.pack_start(Bsalir, True, True, 0)
        Vbox.pack_start(Hbox5, True, True, 0)

        self.main_windows.add(Vbox)
        self.main_windows.show_all()
        Gtk.main()

    def close_main(self, arg1):
        """TODO: Docstring for close_main.

        :arg1: TODO
        :returns: TODO

        """
        self.main_windows.hide()
        # Gtk.main_quit()

    def cargar(self, widget):
        """TODO: Docstring for cargar.
        :returns: TODO

        """

        conf_py = """# datos de la conexion
ssid = '{}'
password = '{}'"""
        tty = self.Ttty.get_text()
        baud = self.Tbaud.get_text()
        ssid = self.tconf.get_text()
        contrasena = self.Tpass.get_text()
        conf_py = conf_py.format(ssid, contrasena)
        print(conf_py)
        ruta = os.getenv("HOME") + "/espacio_de_trabajo/config.py" 
        dat = open(ruta, "w")
        dat.writelines(conf_py)
        dat.close()
        cad = "ampy --port {} --baud {} put {}"
        cad = cad.format(tty, baud, ruta)
        print(cad)
        operacion = os.system(cad)
        # print(operacion)
        if operacion == 0:
            cad = "la carga de la configuración, fue exitosa"
        else:
            cad = "Hubo un error en la carga del archivo. COD: "+str(operacion)
        dialog = MENSAJE(self.main_windows, cad)
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            print("The OK button was clicked")
        dialog.destroy()

# arg = CARGADOR()

