#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###############################################################################
#
# Herramienta para investigación en el doctorado en educación en ciencia y
# tecnología
# Copyright © 2019 Valentín Basel <valentinbasel@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

###############################################################################
#     _______  _______  _______  ___      ___   _  _______  ______
#    |       ||       ||   _   ||   |    |   | | ||       ||    _ |
#    |  _____||_     _||  |_|  ||   |    |   |_| ||    ___||   | ||
#    | |_____   |   |  |       ||   |    |      _||   |___ |   |_||_
#    |_____  |  |   |  |       ||   |___ |     |_ |    ___||    __  |
#     _____| |  |   |  |   _   ||       ||    _  ||   |___ |   |  | |
#    |_______|  |___|  |__| |__||_______||___| |_||_______||___|  |_|
#
###############################################################################

###############################################################################
#    _______  _______  ______    _______  _______  _______
#   |       ||       ||    _ |  |       ||   _   ||       |
#   |_     _||   _   ||   | ||  |       ||  |_|  ||____   |
#     |   |  |  | |  ||   |_||_ |       ||       | ____|  |
#     |   |  |  |_|  ||    __  ||      _||       || ______|
#     |   |  |       ||   |  | ||     |_ |   _   || |_____
#     |___|  |_______||___|  |_||_______||__| |__||_______|
#
###############################################################################

# librerias generales
import gi
# import tarfile
gi.require_version('Gtk', '3.0')

from util import ABRIR_DIALOG
from util import NUEVO_DOC
from util import DIALOG_OK_CANCEL
from util import DIALOG_OK
from tab import HOJA
from util import MENSAJE
from util import UTIL
from cargador_micropython import CARGADOR
import template
import subprocess
import os
import time
from datetime import datetime
from gi.repository import Gtk
# import shutil


# librerias internas


class MAIN_W(object):

    """La clase MAIM_W es la clase principal de TORCAZ.
        todos los eventos y la construcción del arbol de widget sale de aca
        el esquema de contenedores es el siguiente:

            main_windows
             |
              --> headerbar
             |       |
             |        --> button
             |       |      |
             |       |       --> popover
             |       |              |
             |       |               --> box_popover
             |       |                     |
             |       |                      --> boton_menu
             |       |                           |
             |       |                            --> box_boton_popover
             |       |                                 |
             |       |                                  --> label
             |       |                                 |
             |       |                                  --> image
             |       |
             |        --> button_tab
             |               |
             |                --> box_boton
             |                       |
             |                        --> image
              --> hbox
                   |
                    --> notebook
    Notas:

    La variable global "grabar", habilita a la clase para poder capturar
    el microfono y la pantalla de video

    """

    def __init__(self, grabar):
        """TODO: to be defined1. """
        # Variable con la declaración, nombre, toolkit y metodo asociado
        # para cada boton del menu
        menues = [("Abrir",
                   "Abre un archivo y agregarlo al proyecto", 'document-open',
                   lambda b: self.abrir()
                   ),
                  ("Abrir proyecto",
                   "Abre un proyecto",
                   'document-open',
                   lambda b: self.abrir_pry()
                   ),
                  ("Nuevo archivo/proyecto",
                   "Crea una nueva pestaña",
                   'document-new',
                   lambda b: self.new_tab(None)
                   ),
                  ("salir",
                   "Salir del programa",
                   'application-exit',
                   lambda b: self.close_main(None)
                   ),
                  ]
        self.proyecto = []  # almacena el nombre de cada archivo py que se crea
        self.grabar = grabar  # Si es True habilita la grabación del escritorio
        # declaro las variables de los archivos de configuración.
        # aca se guardan los CSV de las teclas pulsadas por el teclado
        # los puntos X/Y del mouse y un archivo de configuración para burrhus
        self.mouse_csv = None
        self.archivo_csv = None
        self.stalker_conf = None
        self.sesion = None
        self.ruta_sesion = None
        self.texto_template = template.texto
        self.main_windows = Gtk.Window(title="IDE python")
        self.main_windows.set_default_size(-1, 350)
        self.main_windows.connect("destroy", self.close_main)
        self.main_windows.connect("motion-notify-event", self.mouse_event)
        self.main_windows.show_all()
        self.headerbar = Gtk.HeaderBar()
        if self.grabar:
            self.headerbar.set_title("Torcaz")
            self.headerbar.set_subtitle("el sistema esta grabando el"
                                        " escritorio y el microfono")
        else:
            text = ("el sistema NO esta grabando el " +
                    "escritorio ni el microfono " +
                    "(solo interacciones de teclado dentro del IDE)")
            self.headerbar.set_title("Torcaz")
            self.headerbar.set_subtitle(
                text)
        self.headerbar.set_show_close_button(True)
        self.main_windows.set_titlebar(self.headerbar)
        # boton de barra de herramientas
        button = Gtk.Button("Menu")
        button.connect("clicked", self.on_popover_clicked)
        self.headerbar.pack_start(button)
        self.new_button(
            "document-new",
            "nueva pestaña",
            self.headerbar,
            self.new_tab)
        self.new_button(
            "system-run",
            "micropython",
            self.headerbar,
            self.micropython)
        # popover
        self.popover = Gtk.Popover()
        self.popover.set_relative_to(button)
        # Box donde se guardan los botones del menu popover
        box_popover = Gtk.Box()
        box_popover.set_spacing(5)
        box_popover.set_orientation(Gtk.Orientation.VERTICAL)
        self.popover.add(box_popover)
        # creo los botones que van dentro del popover
        for item in menues:
            text, tooltip, iconname, callback = item
            label = Gtk.Label(text)
            image = Gtk.Image.new_from_icon_name(iconname,
                                                 Gtk.IconSize.LARGE_TOOLBAR
                                                 )
            box_boton_popover = Gtk.Box()
            box_boton_popover.set_homogeneous(False)
            box_boton_popover.set_orientation(Gtk.Orientation.HORIZONTAL)
            box_boton_popover.set_spacing(5)
            box_boton_popover.add(image)
            box_boton_popover.add(label)
            boton_menu = Gtk.Button()
            boton_menu.set_tooltip_text(tooltip)
            boton_menu.add(box_boton_popover)
            boton_menu.connect("clicked", callback)
            box_popover.add(boton_menu)
        self.hbox = Gtk.HBox(spacing=6)
        self.notebook = Gtk.Notebook()
        self.hbox.pack_start(self.notebook, True, True, 0)
        self.main_windows.add(self.hbox)
        self.main_windows.show_all()
        self.main_windows.connect('notify::is-active', self.cambia_el_foco)
        Gtk.main()

    def mouse_event(self, a, b):
        """TODO: Docstring for mouse_event.

        :a: TODO
        :returns: TODO

        """
        if self.mouse_csv is not None:
            fecha = str(datetime.now()).split(" ")
            cadena_final = str(b.x) + "," + str(b.y) + "," + fecha[1] + "," + fecha[0] + "\n"
            self.mouse_csv.write(cadena_final)

    def cambia_el_foco(self, window, param):
        print("cambio de foco")
        if self.archivo_csv is not None:
            ventana_ac = UTIL.obtener_ventana_activa()
            if ventana_ac is not None:
                ventana_ac = ventana_ac.decode()
                fecha = str(datetime.now()).split(" ")
                # cadena_final = "foco_" + \
                # str(window.props.is_active) + "_" + \
                cadena_final = ventana_ac + "," + fecha[1] + \
                    "," + fecha[0] + "\n"
                self.archivo_csv.write(cadena_final)

    def crear_proyecto(self, ruta):
        """TODO: Docstring for crear_proyecto.
        :returns: TODO

        """
        if self.grabar is True:
            codigo = "V"
        else:
            codigo = "T"
        fecha = str(datetime.now()).split(" ")
        fecha_f = fecha[0].split("-")  # fecha foramteada en DD_MM_AAAA
        fecha_ff = fecha_f[2]+"_"+fecha_f[1]+"_"+fecha_f[0]
        fecha_arch = fecha_ff + "x" + fecha[1]
        # self.ruta_trabajo
        fecha_arch = fecha_arch.replace(":", "_")
        fecha_arch = fecha_arch.replace(".", "_")
        self.rutastalker_proy = os.getenv("HOME") + \
            "/espacio_de_trabajo/" + ruta + "/"
        self.ruta_sesion = os.getenv("HOME") + \
            "/espacio_de_trabajo/" + ruta + "/"

        # fuente = os.getenv("HOME") + \
        #    "/espacio_de_trabajo/firmware/domoticaro.tar.gz"

        print(self.rutastalker_proy)
        if os.path.exists(self.rutastalker_proy) is False:
            os.mkdir(self.rutastalker_proy)
            # shutil.copyfile(fuente, self.rutastalker_proy +
            #                "domoticaro.tar.gz")
            # file_untar = self.rutastalker_proy + "domoticaro.tar.gz"
            # tf = tarfile.open(file_untar)
            # tf.extractall(self.rutastalker_proy)
        self.rutastalker = self.rutastalker_proy + codigo+"x"+fecha_arch + "/"
        os.mkdir(self.rutastalker)
        self.stalker_conf = open(self.rutastalker + "stalker.config", "w")
        self.archivo_csv = open(self.rutastalker + "main_windows.csv", "w")
        self.archivo_csv.write("ventana,hora,fecha\n")
        self.mouse_csv = open(self.rutastalker + "main_windows_mouse.csv", "w")
        self.mouse_csv.write("x,y,hora,fecha\n")
        sesion = os.getenv("HOME") + "/espacio_de_trabajo/"+ruta+"/" +\
            ruta+".trcz"
        self.sesion = open(sesion, "w")
        if self.grabar:
            self.desktop_record_start()

    def abrir(self):
        """TODO: Docstring for abrir.
        :returns: TODO

        """
        if len(self.proyecto) <= 0:
            print("no se puede ")
            return
        else:
            pry = ABRIR_DIALOG()
            resultado = pry.abrir_pry("python")
            if resultado is None:
                return
            direc, archi = UTIL.split_archivo(None, resultado)
            print(direc)
            print(archi)
            nombre = archi.strip(".py")
            for dat in self.proyecto:
                if dat == nombre:
                    MENSAJE("el archivo ya esta abierto", "ERROR")
                    return

            ruta_archivo = direc+archi
            arch = open(ruta_archivo, "r")
            arch_tex = arch.readlines()
            lineas = ""
            for linea in arch_tex:
                lineas = lineas + linea
            arch.close()

            self.agr_nom_proyecto(nombre, lineas)

    def abrir_pry(self):
        """TODO: Docstring for abrir.
        :returns: TODO

        """
        print("abrir proyecto")
        pry = ABRIR_DIALOG()
        resultado = pry.abrir_pry("proyecto")
        if resultado is None:
            return
        direc, archi = UTIL.split_archivo(None, resultado)
        print(direc)
        print(archi)
        if resultado is not None:  # todo esto hay que revisar
            sesion = open(resultado, "r")
            tabs = []
            for t in sesion.readlines():
                tabs.append(t)
            sesion.close()
            print(tabs)
            self.crear_proyecto(archi.strip(".trcz"))  # esto esta mal

            for tab in tabs:
                tab = tab.strip("\n")
                ruta_archivo = direc+tab+".py"
                arch = open(ruta_archivo, "r")
                arch_tex = arch.readlines()
                lineas = ""
                for linea in arch_tex:
                    lineas = lineas + linea
                arch.close()
                self.agr_nom_proyecto(tab, lineas)

    def agr_nom_proyecto(self, nombre, texto):
        """TODO: Docstring for agr_nom_proyecto.
        :nombre: TODO
        :returns: TODO

        """
        self.proyecto.append(nombre)
        self.sesion.write(nombre+"\n")
        HOJA(self.notebook, nombre,
             self.rutastalker,
             self.rutastalker_proy,
             self.main_windows,
             texto,
             self.proyecto
             )
        self.stalker_conf.write(nombre + ".csv\n")
        self.main_windows.show_all()

    def new_tab(self, button):
        """TODO: Docstring for new_tab.
        :returns: TODO

        """
        print(self.proyecto)
        nuevo_arch = NUEVO_DOC(self.main_windows)
        if len(self.proyecto) > 0:

            nombre = nuevo_arch.run("Elija un nombre para su archivo",
                                    "nuevo archivo")
            if nombre is None:
                return
            for dat in self.proyecto:
                if dat == nombre:
                    MENSAJE("nombre duplicado", "ERROR")
                    return
            self.agr_nom_proyecto(nombre, self.texto_template)

        else:
            # NUEVO_DOC(self.main_windows)
            nombre = nuevo_arch.run("Elija un nombre para el proyecto",
                                    "nuevo proyecto")
            if nombre is None:
                return
            self.crear_proyecto(str(nombre))
            nuevo_arch = NUEVO_DOC(self.main_windows)
            nombre = nuevo_arch.run("Elija un nombre para su archivo",
                                    "nuevo archivo")
            if nombre is None:
                return
            self.agr_nom_proyecto(nombre, self.texto_template)

    def on_popover_clicked(self, button):
        self.popover.show_all()

    def desktop_record_start(self):
        """TODO: Docstring for grabacion.
        :returns: TODO

        """

        fecha = str(datetime.now()).split(" ")
        cadena_final = fecha[1] + "_" + fecha[0]
        cadena_final = cadena_final.replace(".", "_")

        cadena_final = cadena_final.replace(";", "_")
        cadena_final = cadena_final.replace(":", "_")

        cadena_final_grabacion = self.rutastalker + cadena_final + ".mkv"
        xp, yp = self.tam_pant()
        cad_pant = "ffmpeg -video_size " + str(xp) + "x" + str(yp) + " "
        #  "ffmpeg -video_size 800x480 "
        cadena = [cad_pant,
                  "-framerate 25 -f x11grab ",
                  "-i :0.0+0,0 -f alsa -ac 2 -i ",
                  "default ", cadena_final_grabacion, " -loglevel quiet"]
        video = ""
        for linea in cadena:
            video = video + linea
        print(video)
        self.proc1 = subprocess.Popen(video, shell=True)
        self.stalker_conf.write(cadena_final + ".mkv\n")

    def tam_pant(self):
        size = (None, None)
        args = ["xrandr", "-q", "-d", ":0"]
        proc = subprocess.Popen(args, stdout=subprocess.PIPE)
        for line in proc.stdout:
            if isinstance(line, bytes):
                line = line.decode("utf-8")
                if "Screen" in line:
                    size = (int(line.split()[7]),  int(line.split()[9][:-1]))
        return size

    def record_desktop_stop(self):
        """TODO: Docstring for record_desktop_stop.
        :returns: TODO

        """
        time.sleep(1)
        subprocess.Popen.kill(self.proc1)
        time.sleep(2)
        print(os.system("killall -INT ffmpeg"))
        time.sleep(1)

    def cerrar_archivos(self):
        """TODO: Docstring for cerrar_archivos.
        :returns: TODO

        """
        if self.grabar is True:
            self.record_desktop_stop()
        if self.archivo_csv is not None:
            self.archivo_csv.close()
        if self.mouse_csv is not None:
            self.mouse_csv.close()
        if self.stalker_conf is not None:
            self.stalker_conf.write("main_windows.csv\n")
            self.stalker_conf.close()
        if self.sesion is not None:
            self.sesion.close()

    def close_main(self, arg1):
        """TODO: Docstring for close_main.

        :arg1: TODO
        :returns: TODO

        """
        self.cerrar_archivos()
        arch_tar = self.rutastalker.split("/")[-2] + ".tar.gz"
        arch_tar = self.ruta_sesion + arch_tar
        result = subprocess.call(['tar', '-czf', arch_tar, self.rutastalker])
        print(self.rutastalker)
        print(arch_tar)
        print(result)
        Gtk.main_quit()

    def new_button(self, imagen, text, headerbar, func):
        """TODO: Docstring for botones_nuevo.

        :arg1: TODO
        :returns: TODO

        """
        # boton de nueva pestaña
        button_tab = Gtk.Button()
        iconSize = Gtk.IconSize.LARGE_TOOLBAR
        img = imagen
        image = Gtk.Image.new_from_icon_name(img, iconSize)
        # label = Gtk.Label(text)

        box_boton = Gtk.VBox()
        box_boton.set_homogeneous(False)
        # box_boton.set_orientation(Gtk.Orientation.VERTICAL)
        box_boton.set_spacing(5)
        box_boton.add(image)
        # box_boton.add(label)
        button_tab.add(box_boton)
        button_tab.connect("clicked", func)
        headerbar.pack_start(button_tab)

    def micropython(self, arg1):
        """TODO: Docstring for micropython.

        :arg1: TODO
        :returns: TODO

        """
        CARGADOR()


mensaje = DIALOG_OK_CANCEL(None, "advertencia", template.disclaimer)
respuesta = mensaje.run()
if respuesta == Gtk.ResponseType.OK:
    grabar = True
elif respuesta == Gtk.ResponseType.CANCEL:
    grabar = False
mensaje.destroy()
win = MAIN_W(grabar)
