#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###############################################################################
# 
# Copyright © 2021 Valentin Basel <valentinbasel@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################

# librerias generales
import gi
# import tarfile
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import subprocess
import os
import time
from datetime import datetime
import pynput.keyboard as Keyboard
from pynput import mouse


class VENTANA(object):

    """Docstring for VENTANA. """

    def __init__(self):
        """TODO: to be defined. """
        # Variables de la clase
        self._grabar = False
        self.mouse_csv = None
        self.archivo_csv = None
        self.stalker_conf = None
        self.sesion = None
        self.ruta_sesion = None

        self.main_windows = Gtk.Window(title="Torcaz")
        self.main_windows.set_default_size(-1, 150)
        #self.main_windows.connect("destroy", self.close_main)
        #self.main_windows.connect("motion-notify-event", self.mouse_event)
        self.main_windows.show_all()
        self.headerbar = Gtk.HeaderBar()
        hbox = Gtk.HBox()
        boton_ini = Gtk.Button(label = "iniciar")
        boton_ini.connect("clicked", self.iniciar)
        hbox.pack_start(boton_ini,0,False,False)
        self.main_windows.add(hbox)

        self.headerbar.set_show_close_button(True)
        self.main_windows.set_titlebar(self.headerbar)
        self.main_windows.show_all()
        #self.main_windows.connect('notify::is-active', self.cambia_el_foco)
        Gtk.main()
    def iniciar(self, widget):
        """TODO: Docstring for iniciar.

        :arg1: TODO
        :returns: TODO

        """
        if self._grabar == False:
            widget.props.label="Parar"
            self._grabar = True
             
            k = Keyboard.Listener(on_press=self.on_press, 
                                  on_release=self.on_release)
            
            k.start()
            
            #key_listener = keyboard.Listener(on_release=on_functionf8)
            #key_listener.start()
            # Collect events until released
            with mouse.Listener(
                    on_move=self.on_move,
                    on_click=self.on_click,
                    on_scroll=self.on_scroll) as listener:
                listener.join()


        else:
            widget.props.label="iniciar"
            self._grabar = False

    def on_press(self,key):
        # Callback function whenever a key is pressed
        try:
            print(f'Key {key.char} pressed!')
        except AttributeError:
            print(f'Special Key {key} pressed!')
     
    def on_release(self,key):
        print(f'Key {key} released')
        if key == Keyboard.Key.esc:
            # Stop the listener
            return False


    def on_move(self,x, y):
        print('Pointer moved to {0}'.format(
            (x, y)))

    def on_click(self,x, y, button, pressed):
        #print("boton")
        print('{0} at {1}'.format(
            'Pressed' if pressed else 'Released',
            (x, y)))
        #if not pressed:
            # Stop listener
        #    return False

    def on_scroll(self,x, y, dx, dy):
        print('Scrolled {0} at {1}'.format(
            'down' if dy < 0 else 'up',
            (x, y)))



torcaz = VENTANA()
