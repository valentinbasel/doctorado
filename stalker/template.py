#!/usr/bin/env python3
# -*- coding: utf-8 -*-

texto = """#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import turtle
### variables para manejo de python - turtle en español
###############################################################################
adelante = turtle.forward
atras = turtle.backward
izquierda = turtle.left
derecha = turtle.right
###############################################################################

## no escribas despues de esta linea
input("presiona ENTER para continuar")
"""

disclaimer = """
Advertencia

TORCAZ es un software para propósitos de investigación, el sistema grabara toda 
iteración dentro del entorno, clicks de mouse, presión de teclas,  perdida de 
foco de la ventana principal del IDE.

Si aprietas “ACEPTAR”,  también se grabara un video con una filmación del 
escritorio + el micrófono de la computadora.

Si aprietas “CANCELAR”  se desactivara el sistema de grabación de micrófono y 
escritorio, solo guardando las iteraciones de teclado y mouse que se hayan 
hecho dentro del entorno TORCAZ.
"""
