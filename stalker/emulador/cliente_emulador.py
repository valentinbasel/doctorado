#!/usr/bin/env python3
# -*- coding: utf-8 -*-


###############################################################################
# emulador de chatbot para domoticaro
# Copyright © 2020 Valentin Basel <valentinbasel@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################

import gi
gi.require_version('Gtk', '3.0')
import threading
import time
import os
from gi.repository import Gtk



class MENSAJERO(Gtk.Window):

    def __init__(self, arg=None):
        Gtk.Window.__init__(self, title="Emulador de chat para domoticaro")
        self.set_size_request(200, 100)
        self.chat_id = 123456
        self.mensajes = []
        self._num_act = 0
        self._diferencia = 0
        self.ruta = os.getenv("HOME") + "/espacio_de_trabajo/emulador/"
        arch2 = open(self.ruta + "log2.txt", "w")
        arch2.close()
        arch = open(self.ruta + "log1.txt", "w")
        arch.close()
        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.add(vbox)
        self.entry = Gtk.Entry()
        self.entry.set_text("Hello World")
        scrolledwindow = Gtk.ScrolledWindow()
        scrolledwindow.set_hexpand(True)
        scrolledwindow.set_vexpand(True)
        vbox.pack_start(scrolledwindow, True, True, 0)
        self.textview = Gtk.TextView()
        self.textbuffer = self.textview.get_buffer()
        self.textbuffer.set_text("Esto es un emulador de chat para testing \n")
        scrolledwindow.add(self.textview)
        vbox.pack_start(self.entry, False, False, 0)
        self.button = Gtk.Button(label="enviar")
        self.button.connect("clicked", self.on_button_enviar)
        vbox.pack_start(self.button, False, False, 0)
        self.connect("destroy", Gtk.main_quit)
        self.show_all()
        thread = threading.Thread(target=self.actualizar)
        thread.daemon = True
        thread.start()
        Gtk.main()

    def hay_mensajes(self):
        num = len(self.mensajes)
        self._diferencia = num - self._num_act
        if num > self._num_act:

            print(self._diferencia)
            self._num_act = num
            return True
        else:
            return False

    def actualizar(self):
        """TODO: Docstring for actualizar.
        :returns: TODO

        """
        while True:
            mensajes = []
            arch2 = open(self.ruta + "log2.txt", "r")
            for a in arch2.readlines():
                mensajes.append(a)
            arch2.close()
            self.mensajes = mensajes
            if self.hay_mensajes():
                for a in range(self._diferencia*-1, 0):
                    chat_id, mens = self.obtener_ultimo_mensaje(a)
                    self.enviar_mensaje(chat_id, mens)
            time.sleep(1)

    def obtener_ultimo_mensaje(self, a):
        mens = self.mensajes[a]  # ultima_act]
        chat_id = self.chat_id
        return chat_id, mens

    def enviar_mensaje(self, chat_id, mens):
        cadena = "BOT: " + mens + "\n"
        end_iter = self.textbuffer.get_end_iter()
        self.textbuffer.insert(end_iter, cadena)

    def on_button_enviar(self, widget):
        mens = self.entry.get_text()
        cadena = str(self.chat_id) + ": " + mens + "\n"
        end_iter = self.textbuffer.get_end_iter()
        self.textbuffer.insert(end_iter, cadena)
        self.mensajes.append(mens)
        arch = open(self.ruta + "log1.txt", "a")
        arch.write("\n" + mens)
        arch.close()

    def limpiar_buffer(self):
        """TODO: Docstring for limpiar_buffer.
        :returns: TODO

        """
        start = self.textbuffer.get_start_iter()
        end = self.textbuffer.get_end_iter()
        self.textbuffer.remove_all_tags(start, end)

win = MENSAJERO()
