#! /usr/bin/python

#
# gtk3 example/widget for VLC Python bindings
# Copyright (C) 2017 Olivier Aubert <contact@olivieraubert.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
#

###############################################################################
#    _______  __   __  ______    ______    __   __  __   __  _______
#   |  _    ||  | |  ||    _ |  |    _ |  |  | |  ||  | |  ||       |
#   | |_|   ||  | |  ||   | ||  |   | ||  |  |_|  ||  | |  ||  _____|
#   |       ||  |_|  ||   |_||_ |   |_||_ |       ||  |_|  || |_____
#   |  _   | |       ||    __  ||    __  ||       ||       ||_____  |
#   | |_|   ||       ||   |  | ||   |  | ||   _   ||       | _____| |
#   |_______||_______||___|  |_||___|  |_||__| |__||_______||_______|
#
###############################################################################
"""VLC Gtk3 Widget classes + example application.

This module provides two helper classes, to ease the embedding of a
VLC component inside a pygtk application.

VLCWidget is a simple VLC widget.

DecoratedVLCWidget provides simple player controls.

When called as an application, it behaves as a video player.
"""

import gi
import time
gi.require_version('Gdk', '3.0')
#
from gettext import gettext as _
import vlc
import sys
from lista import TreeViewFilterWindow
from gi.repository import Gtk, GObject
from gi.repository import Gdk
#gi.require_version('Gtk', '3.0')
Gdk.threads_init()


# Create a single vlc.Instance() to be shared by (possible) multiple players.
if 'linux' in sys.platform:
    # Inform libvlc that Xlib is not initialized for threads
    instance = vlc.Instance("--no-xlib")
else:
    instance = vlc.Instance()


class ANOTADOR(object):

    """Docstring for ANOTADOR. """

    def __init__(self):
        """TODO: to be defined. """

        cad = "anotar experiencias para autoconfrontación"
        self.main_windows = Gtk.Window(title=cad)
        self.main_windows.set_default_size(-1, 350)
        self.dir = ""
        # ID
        hbox1 = Gtk.HBox()
        vbox1 = Gtk.VBox()
        l1 = Gtk.Label(label="ID")
        self.entry_id = Gtk.Entry()
        hbox1.pack_start(l1, True, True, 0)
        hbox1.pack_start(self.entry_id, True, True, 0)
        vbox1.pack_start(hbox1, False, False, 0)
        # inicio
        hbox2 = Gtk.HBox()
        l2 = Gtk.Label(label="Inicio")
        self.entry_inic = Gtk.Entry()
        hbox2.pack_start(l2, True, True, 0)
        hbox2.pack_start(self.entry_inic, True, True, 0)
        vbox1.pack_start(hbox2, False, False, 0)
        # fin
        hbox3 = Gtk.HBox()
        l3 = Gtk.Label(label="final")
        self.entry_fin = Gtk.Entry()
        hbox3.pack_start(l3, True, True, 0)
        hbox3.pack_start(self.entry_fin, True, True, 0)
        vbox1.pack_start(hbox3, False, False, 0)
        # SRT
        hbox4 = Gtk.HBox()
        l4 = Gtk.Label(label="SRT")
        self.entry_srt = Gtk.Entry()
        hbox4.pack_start(l4, True, True, 0)
        hbox4.pack_start(self.entry_srt, True, True, 0)
        vbox1.pack_start(hbox4, False, False, 0)
        # Boton agregar y salir
        hbox5 = Gtk.HBox()
        bto_salir = Gtk.Button("Salir")
        bto_agregar = Gtk.Button("Agregar")
        hbox5.pack_start(bto_agregar, True, True, 0)
        hbox5.pack_start(bto_salir, True, True, 0)
        vbox1.pack_start(hbox5, False, False, 0)
        self.main_windows.add(vbox1)

        bto_agregar.connect("clicked", self.agregar)

    def agregar(self, widget):
        """TODO: Docstring for agregar.
        :returns: TODO

        """
        id_srt = self.entry_id.get_text()
        ini = self.entry_inic.get_text()
        fin = self.entry_fin.get_text()
        srt = self.entry_srt.get_text()
        cadena = id_srt + "\n"
        cadena = cadena + ini + " --> " + fin + "\n"
        cadena = cadena + srt + "\n" + "\n"
        fil = self.dir + "sub.srt"
        # print(fil)
        archivo = open(fil, "a")
        archivo.write(cadena)
        archivo.close()
        print(cadena)

    def hhmmss(self, segundostotales):
        mmm = segundostotales % 1000
        segundostotales = segundostotales // 1000
        hh = segundostotales // 36000
        mm = (segundostotales % 36000)//60
        ss = (segundostotales % 36000) % 60
        tiempo = str(hh) + ":" + str(mm) + ":" + str(ss) + "." + str(mmm)
        self.entry_id.set_text(str(segundostotales))
        self.entry_inic.set_text(tiempo)
        self.entry_fin.set_text(tiempo)
        return True


class VLCWidget(Gtk.DrawingArea):
    """Simple VLC widget.

    Its player can be controlled through the 'player' attribute, which
    is a vlc.MediaPlayer() instance.
    """
    __gtype_name__ = 'VLCWidget'

    def __init__(self, *p):
        Gtk.DrawingArea.__init__(self)
        self.player = instance.media_player_new()
        self.tp = (1300, 600)
        self.directorio_trabajo = ""

        def handle_embed(*args):
            if sys.platform == 'win32':
                self.player.set_hwnd(self.get_window().get_handle())
            else:
                self.player.set_xwindow(self.get_window().get_xid())
            return True
        self.connect("realize", handle_embed)
        self.set_size_request(self.tp[0], self.tp[1])
        self.connect("draw", self.da_draw_event)

    def da_draw_event(self, widget, cairo_ctx):
        cairo_ctx.set_source_rgb(0, 0, 0)
        cairo_ctx.paint()

    def grabar_img(self):
        window = self.get_window()
        # Fetch what we rendered on the drawing area into a pixbuf
        pixbuf = Gdk.pixbuf_get_from_window(window,
                                            0,
                                            0,
                                            self.tp[0],
                                            self.tp[1])
        # Write the pixbuf as a PNG image to disk
        tiempo = time.strftime("%H_%M_%S-%d_%m_%y")
        archivo = self.directorio_trabajo + "img_"+tiempo+".png"
        pixbuf.savev(archivo, 'png', [], [])


class DecoratedVLCWidget(Gtk.VBox):
    """Decorated VLC widget.

    VLC widget decorated with a player control toolbar.

    Its player can be controlled through the 'player' attribute, which
    is a Player instance.
    """
    __gtype_name__ = 'DecoratedVLCWidget'

    def __init__(self, *p):
        super(DecoratedVLCWidget, self).__init__()
        self._vlc_widget = VLCWidget(*p)
        self.player = self._vlc_widget.player
        ad1 = Gtk.Adjustment(0, 0, 100, 1, 20, 0)
        self.scaled = Gtk.Scale(
            orientation=Gtk.Orientation.HORIZONTAL,
            adjustment=ad1)
        self.scaled.connect("button-release-event", self.scale_moved)
        self.pack_start(self._vlc_widget, True, True, 0)
        self.pack_start(self.scaled, False, False, 0)
        self._toolbar = self.get_player_control_toolbar()
        # self.pack_start(self._toolbar, False, False, 0)
        hbox = Gtk.HBox()
        label_text = Gtk.Label("multiplicador de avance/retroceso (en Ms)")
        self.segundero = Gtk.Entry(text="1000")
        self.label = Gtk.Label("0.0")
        boton_cap = Gtk.Button(label="capt_img")
        boton_srt = Gtk.Button(label="anotar")
        boton_srt.connect("clicked", self.boton_anotar_srt)
        boton_cap.connect("clicked", self.boton_cap_img)

        hbox.pack_start(self._toolbar, False, False, 0)
        hbox.pack_start(label_text, False, False, 0)
        hbox.pack_start(self.segundero, False, False, 0)
        hbox.pack_start(self.label, False, False, 0)
        hbox.pack_start(boton_cap, False, False, 0)
        hbox.pack_start(boton_srt, False, False, 0)
        self.pack_start(hbox, False, False, 0)
        # self.add(self.vbox)
        GObject.timeout_add(1000, self.on_timeout, None)
        self.show_all()
        self.anotador = ANOTADOR()

    def scale_moved(self, a, b):
        """TODO: Docstring for scale_moved.

        :arg1: TODO
        :returns: TODO

        """
        s = self.player.get_state()
        if s != vlc.State.NothingSpecial and self.player.get_time() > -1:
            t = self.player.get_time()
            print(t)
            segundos = a.get_value()
            print(int(segundos))
            self.player.set_time(int(segundos)*1000)

    def boton_anotar_srt(self, arg1):
        """TODO: Docstring for boton_anotar_srt.

        :arg1: TODO
        :returns: TODO

        """
        s = self.player.get_state()
        if s != vlc.State.NothingSpecial and self.player.get_time() > -1:
            t = self.player.get_time()
            self.player.pause()
            print("anoto: ", t)
            self.anotador.dir = self._vlc_widget.directorio_trabajo
            self.anotador.hhmmss(t)
            self.anotador.main_windows.show_all()

    def boton_cap_img(self, arg1):
        """TODO: Docstring for boton_cap_img.

        :arg1: TODO
        :returns: TODO

        """
        print("boton")
        self._vlc_widget.grabar_img()

    def on_timeout(self, arg1):
        """TODO: Docstring for on_timeout.

        :arg1: TODO
        :returns: TODO

        """
        s = self.player.get_state()
        if s != vlc.State.NothingSpecial and self.player.get_time() > -1:
            t = self.player.get_time()
            # print(t)
            tamaño = self.player.get_length()
            if t > 0:
                mm = float(int(t) * 100 / int(tamaño))
                # print(mm)
                self.scaled.set_value(mm)
                self.label.set_text("tiempo: "+str(float(t/1000)))
        return True

    def get_player_control_toolbar(self):
        """Return a player control toolbar
        """
        tb = Gtk.Toolbar.new()
        for text, tooltip, iconname, callback in (
            ("Play", "Play", 'media-playback-start',
             lambda b: self.player.play()),
            (_("Pause"), _("Pause"), 'media-playback-pause',
             lambda b: self.player.pause()),
            (_("Stop"), _("Stop"), 'media-playback-stop',
             lambda b: self.player.stop()),
            (_("Quit"), _("Quit"), 'window-close-symbolic', Gtk.main_quit),
            (_("-1S"), _("-1S"), 'media-skip-backward', self.S_menos_1),
            (_("+1S"), _("+1S"), 'media-skip-forward', self.S_mas_1),
        ):
            i = Gtk.Image.new_from_icon_name(
                iconname, Gtk.IconSize.LARGE_TOOLBAR)
            b = Gtk.ToolButton()  # i, text)
            b.set_icon_widget(i)
            b.set_tooltip_text(tooltip)
            b.connect("clicked", callback)
            tb.insert(b, -1)
        return tb

    def S_mas_1(self, arg1):
        """TODO: Docstring for S1.

        :arg1: TODO
        :returns: TODO

        """
        print("+1S")
        s = self.player.get_state()
        if s != vlc.State.NothingSpecial and self.player.get_time() > -1:
            t = self.player.get_time()
            segundos = self.segundero.get_text()
            self.player.set_time(t + int(segundos))

        return True

    def S_menos_1(self, arg1):
        """TODO: Docstring for S1.

        :arg1: TODO
        :returns: TODO

        """
        print("-1S")
        s = self.player.get_state()
        if s != vlc.State.NothingSpecial and self.player.get_time() > -1:
            t = self.player.get_time()
            segundos = self.segundero.get_text()
            self.player.set_time(t - int(segundos))
        return True


class VideoPlayer:
    """Example simple video player.
    """

    def __init__(self):
        self.vlc = DecoratedVLCWidget()

    def carga_archivos(self, arg1):
        """TODO: Docstring for carga_archivos.

        :arg1: TODO
        :returns: TODO

        """
        arc = open(arg1, "r")
        datos = []
        for texto in arc.readlines():
            datos.append(texto.strip("\n"))
        return datos

    def main(self, fname):
        """
        En main esta el arbol de widget que construye la vantana principal
        de Burrhus

        w
        |
         --> hbox
              |
               --> frame
                    |
                     --> boxx
                    |      |
                    |       --> self.vlc
                    |
                     --> vbox
                          |
                           --> self.notebook_tree
                          |
                           --> boton
        """
        w = Gtk.Window(title="video")
        w2 = Gtk.Window(title="control")
        if fname is None:
            dialog = Gtk.FileChooserDialog(
                                        title="Please choose a file",
                                        parent=w,
                                        action=Gtk.FileChooserAction.OPEN)
            dialog.add_buttons(
                Gtk.STOCK_CANCEL,
                Gtk.ResponseType.CANCEL,
                Gtk.STOCK_OPEN,
                Gtk.ResponseType.OK,
            )
            response = dialog.run()
            if response == Gtk.ResponseType.OK:
                fname = dialog.get_current_folder()+"/"
            elif response == Gtk.ResponseType.CANCEL:
                dialog.destroy()
                exit()
            dialog.destroy()
        self.vlc._vlc_widget.directorio_trabajo = fname
        fname_full = fname + "stalker.config"
        frame = Gtk.Frame(label="video")
        datos = self.carga_archivos(fname_full)
        datos[0] = fname + datos[0]
        fvideo = datos[0].split("/")
        fvideo = fvideo[-1]
        self.seg = self.convert_time(fvideo)
        self.vlc.player.set_media(instance.media_new(datos[0]))
        hbox = Gtk.HBox()
        vbox = Gtk.VBox()
        self.notebook_tree = Gtk.Notebook()
        boton = Gtk.Button(label="PDI")
        boton.connect("clicked", self.boton_click)
        self.pagina_tree = []
        for a in range(1, len(datos)):
            self.tree = TreeViewFilterWindow(fname + datos[a])
            self.notebook_tree.append_page(self.tree)
            nt = self.notebook_tree.get_nth_page(a - 1)
            self.notebook_tree.set_tab_label_text(nt, datos[a])
            self.pagina_tree.append(self.tree)
        boxx = Gtk.HBox()
        boxx.pack_start(self.vlc, True, True, 0)
        frame.add(boxx)
        hbox.pack_start(frame, True, True, 0)
        vbox.pack_start(self.notebook_tree, True, True, 0)
        vbox.pack_start(boton, False, False, 0)
        w2.add(vbox)
        w.add(hbox)
        w2.show_all()
        w.show_all()
        w.connect("destroy", Gtk.main_quit)
        w.connect("key-press-event", self.on_key_press_event)
        Gtk.main()

    def on_key_press_event(self, widget, event):
        """TODO: Docstring for on_key_press_event.

        :arg1: TODO
        :returns: TODO

        """
        print("Key val, name: ", event.keyval, Gdk.keyval_name(event.keyval))
        valor_tecla = Gdk.keyval_name(event.keyval)
        if valor_tecla == "F1":
            self.vlc.player.play()
        if valor_tecla == "space":
            self.vlc.player.pause()
        if valor_tecla == "F3":
            self.vlc.player.stop()
        if valor_tecla == "F4":
            self.boton_click(None)

    def boton_click(self, arg1):
        """TODO: Docstring for boton_click.

        :arg1: TODO
        :returns: TODO

        """

        PDI = self.pagina_tree[self.notebook_tree.get_current_page()].seg
        if PDI > 0:
            pdi = PDI - self.seg
            print(pdi)
            self.vlc.player.set_time(pdi * 1000)

    def convert_time(self, hms):
        """TODO: Docstring for convert_time.

        :arg1: TODO
        :returns: TODO

        """

        hms = hms.split("_")
        h = int(hms[0]) * 3600
        m = int(hms[1]) * 60
        s = int(hms[2])
        seg = h + m + s
        print(seg)
        return seg


if __name__ == '__main__':
    if not sys.argv[1:]:
        p = VideoPlayer()
        p.main(None)
    if len(sys.argv[1:]) == 1:
        p = VideoPlayer()
        p.main(sys.argv[1])
    instance.release()
