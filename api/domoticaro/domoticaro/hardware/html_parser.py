#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###############################################################################
# capa de abstracción para manejar el html generado por la placa ESP17f weemos
# Copyright © 2020 Valentin Basel <valentinbasel@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################


class PARSER(object):

    """Docstring for PARSER. """

    def __init__(self):
        """TODO: to be defined. """
        self._url = ""
        self._dicc_html = {'REL': '', 'R': '', 'G': '', 'B': ''}

    def recibir(self, arg1):
        """TODO: Docstring for recibir.

        :arg1: TODO
        :returns: TODO

        """
        
    
