#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###############################################################################
# Prueba de la API para telegram, prueba_telegram.py
# Copyright © 2020 Valentin Basel <valentinbasel@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################

from bot.bot import BOT
from bot.eliza import ELIZA
import time

bot = BOT('emulador')
eli = ELIZA()

bot.enviar_texto(123456, "hola soy un emulador de ELIZA para pruebas")
while True:
    if bot.hay_mensaje() is True:
        chat_id, texto = bot.recibir_ultimo_mensaje()
        print("el chat es: ", texto)
        respuesta = eli.analyze(texto)
        print("la respuesta es: ", respuesta)
        bot.enviar_texto(chat_id, respuesta)
    time.sleep(0.1)
