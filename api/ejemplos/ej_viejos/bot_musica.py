#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###############################################################################
# bot de telegram para ejecutar musica de youtve en el TV
# Copyright © 2021 Valentin Basel <valentinbasel@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################

#    Para subir el volumen (por ejemplo 10%): amixer sset Master 10%+ 
#    Para bajar el volumen (por ejemplo 5%):  amixer sset Master 5%- 
#    Para activar silencio:                   amixer sset Master mute 
#    Para desactivar silencio:                amixer sset Master unmute 
#    Para activar/desactivar silencio:        amixer sset Master toggle 

from bot.bot import BOT
from clave import token
import time
import os

bot = BOT('telegram', token)


def espero_msg():
    """TODO: Docstring for espero_msg.
    :returns: texto recogido

    """
    Band = True
    while Band:
        if bot.hay_mensaje() is True:
            chat_id, texto = bot.recibir_ultimo_mensaje()
            print("el chat es: ", texto)
            Band = False
        time.sleep(0.1)
    return (chat_id, texto)

def video():
    """TODO: Docstring for video.
    :returns: TODO

    """
    chat_id,texto = espero_msg()
    print("pongo el video: ", texto)
    cad = "google-chrome " + texto + " &"
    os.system("killall -9 chrome")
    os.system(cad)
    bot.enviar_texto(chat_id, "reproduzco el sig video")

def musica(chat_id):
    """TODO: Docstring for musica.
    :returns: TODO

    """
    dir_m = '/home/vbasel/Música/'
    with os.scandir(dir_m) as ficheros:
        ficheros = [fichero.name for fichero in ficheros if fichero.is_dir()]
    band = True
    a = 0
    while band:
        for n in range(10):
            ff = str(n) + " - " + ficheros[a+n]
            bot.enviar_texto(chat_id, ff)

        bot.enviar_texto(chat_id, "99 - más")

        bot.enviar_texto(chat_id, "100 - salir")
        chat_id,texto = espero_msg()
        texto = int(texto)
        if texto == 99:
            a = a+n
            bot.enviar_texto(chat_id, "mas canciones")
        elif texto <99:
            tex = "cvlc " + dir_m + ficheros[texto+a] + "/ &"
            print(tex)
            os.system(tex)
            bot.enviar_texto(chat_id, "ejecuto "+ ficheros[texto+a])
            band = False
        elif texto == 100:
            band = False

while True:
    chat_id,texto = espero_msg()
    texto = texto.lower()
    if texto.find("video") >= 0: 
        bot.enviar_texto(chat_id, "envia el link a reproducir")
        video()
    elif texto.find("musica") >=0:
        musica(chat_id)
        print("musica")
        

