#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import domoticaro

habitacion = domoticaro.iniciar("weemos")
habitacion.iniciar("http://192.168.100.20/")


for t in range(4):
    print("el valor del sensor analogico es: ", habitacion.analogico.leer())
    print("el valor del sensor humedad es: ", habitacion.humedad.leer())
    print("el valor del sensor temp es: ", habitacion.temperatura.leer())
    print("estado del RELE_1: ", habitacion.rele1.conmutar())
    print("estado del RELE_2: ", habitacion.rele2.conmutar())
habitacion.cerrar()
