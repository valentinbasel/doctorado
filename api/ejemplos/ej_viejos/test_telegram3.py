#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###############################################################################
# Prueba de la API para telegram, prueba_telegram.py
# Copyright © 2020 Valentin Basel <valentinbasel@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################

from bot.bot import BOT
from clave import token
from clave import usuario
import time
import domoticaro
import os
import random

habitacion = domoticaro.iniciar("weemos")
habitacion.iniciar("http://192.168.0.141/")
bot = BOT('telegram', token)
bot.enviar_texto(usuario, "hola soy un bot que prende y apaga un RELE")
tiempo = 0  # valor para calcular tiempo transcurrido


def enviar_tyh(bot, chat_id):
    """
    esta función envia los datos de humedad y temp al usuario
    """
    temp = habitacion.temperatura.leer()
    hum = habitacion.Humedad.leer()
    cadena_t = "la temperatura es " + str(temp)
    bot.enviar_texto(chat_id, cadena_t)
    cadena_h = "la humedad es " + str(hum)
    bot.enviar_texto(chat_id, cadena_h)


while True:
    if bot.hay_mensaje() is True:
        chat_id, texto = bot.recibir_ultimo_mensaje()
        print(texto, chat_id)
        print(type(texto))
        if isinstance(texto, str):
            texto = texto.lower()
            if texto.find("agua") > -1:
                bot.enviar_texto(chat_id, "comienzo a calentar el agua")
                bot.enviar_texto(chat_id, "por favor espera 150 segundos, yo te avisare cuando ya este listo")
                bot.enviar_texto(chat_id, "para que no te aburras enviare memasos cada 10 segundos")
                habitacion.Rele_2.on()
                for a in range(15):
                    time.sleep(10)
                    img = random.choice(os.listdir("/home/vbasel/memes"))
                    img = "/home/vbasel/memes/" + img
                    bot.enviar_imagen(chat_id, img)
                bot.enviar_texto(chat_id, "ya calente el agua")
                habitacion.Rele_2.off()
            elif texto.find("ventilador") > -1:
                bot.enviar_texto(chat_id, "conmuto el valor del RELE 1")
                habitacion.Rele_1.conmutar()
            elif texto.find("memes") > -1:
                img = random.choice(os.listdir("/home/vbasel/memes"))
                img = "/home/vbasel/memes/" + img
                bot.enviar_imagen(chat_id, img)
            elif texto.find("temperatura") > -1:
                enviar_tyh(bot, chat_id)
            else:
                bot.enviar_texto(chat_id, "no entiendo")
    time.sleep(0.1)
    # tiempo += 1
    # print(tiempo)
    # if tiempo > 150:
        # print("envio temp")
        # enviar_tyh(bot, usuario)
        # tiempo = 0
