#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###############################################################################
# Prueba de la API para telegram, prueba_telegram2.py
# Copyright © 2020 Valentin Basel <valentinbasel@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################

from bot.bot import BOT
from clave import token  # aca guardamos el TOKEN de telegram
import time

# Creamos el BOT y le decimos que sera una instancia de "telegram"
bot = BOT('telegram', token)

while True:
    # obtengo un Json con los datos que me devuelve telegram 
    datos = bot.actualizar()
    # el Json tiene toda la data que guarda telegram
    # los bots pueden almacenar solo 100 mensajes hasta 24 horas, superado
    # los 100 mensajes, el bot no puede recibir mas mensajes pero si enviar.
    # a las 24 hroas se borran los ultimos mensajes
    print(datos)
    # podemos ver la cantidad de mensajes enviados, usando esta instrucción
    print(len(datos["result"]))
    time.sleep(1)
