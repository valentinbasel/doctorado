#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###############################################################################
# test_icaro.py
# Copyright © 2020 Valentin Basel <valentinbasel@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################

import domoticaro
import time
casa = domoticaro.iniciar("icaro_cdc")
casa.hardware.iniciar("/dev/ttyACM0")
rele1 = casa.hardware.Rele_1
rele2 = casa.hardware.Rele_2
rele3 = casa.hardware.Rele_3
rele4 = casa.hardware.Rele_4
for t in range(17):
    print("prendo")
    casa.hardware.Rele_1.on()
    casa.hardware.Rele_2.on()
    casa.hardware.Rele_3.on()
    casa.hardware.Rele_4.on()
    time.sleep(1)
    print("prendo")
    casa.hardware.Rele_1.off()
    casa.hardware.Rele_2.off()
    casa.hardware.Rele_3.off()
    casa.hardware.Rele_4.off()
    time.sleep(1)
    casa.hardware.cerrar()
