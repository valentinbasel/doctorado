#!/usr/bin/env python3
# -*- coding: utf-8 -*-


###############################################################################
# prueba para inciar un chatbot con pares de (preguntas - respuestas)
# Copyright © 2020 Valentin Basel <valentinbasel@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################

from bot.util import PARES

pares_saludo = [[r'hola|hey', ["¿como andas?",
                               "¿todo bien?"]]]

pares_adios = [[r'chau|nos vemos|adios', ["adios!",
                                          "chau",
                                          "nos vemos, cuidate",
                                          "hasta pronto"]]]

pares_reflexion = [[r'yo (.*)', ["vos %1"]],
                   [r'me gustaria (.*)', ["porque te gustaria %1"]]]

saludo = PARES(pares_saludo)
adios = PARES(pares_adios)
reflexiones = PARES(pares_reflexion)

while True:
    chat = input(">> ")
    r_saludo = saludo.Respuesta(chat)
    r_adios = adios.Respuesta(chat)
    r_reflexiones = reflexiones.Respuesta(chat)
    if r_adios:
        print(r_adios)
        exit()
    elif r_saludo:
        print(r_saludo)
    elif r_reflexiones:
        print(r_reflexiones)
    else:
        print("no entendi")
