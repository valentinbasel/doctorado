#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import domoticaro


###############################################################################
#  Ejemplo de como leer el sensor de humedad/temperatura de la placa domoticaro
###############################################################################

placa = domoticaro.iniciar("weemos")
placa.iniciar("http://192.168.100.140/")


an1 = placa.analogico.leer()  # guardo el valor del sensor analogico
print("el valor del sensor es:", an1)
if an1 > 5.0:  # si el valor es mayor a 5.0, prende el rele si no lo apaga
    placa.rele1.on()
else:
    placa.rele1.off()

