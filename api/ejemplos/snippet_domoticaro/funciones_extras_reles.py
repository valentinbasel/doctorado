#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import domoticaro


###############################################################################
#  ejemplos de herraminetas extras para los RELES de la placa domoticaro
###############################################################################

placa = domoticaro.iniciar("weemos")
placa.iniciar("http://192.168.100.20/")

# Saber el estado de un rele (prendido o apagado)
print("estado del RELE 1:", placa.rele1.estado())
print("estado del RELE 2:", placa.rele2.estado())

# conmutar el valor de un RELE (si esta prendido lo apaga, y si esta 
# apagado, lo prende)

placa.rele1.conmutar()
placa.rele2.conmutar()
