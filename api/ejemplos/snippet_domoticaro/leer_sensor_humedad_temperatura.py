#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import domoticaro


###############################################################################
#  Ejemplo de como leer el sensor de humedad/temperatura de la placa domoticaro
###############################################################################

placa = domoticaro.iniciar("weemos")
placa.iniciar("http://192.168.2.103/")

print("humedad es: ", placa.humedad.leer())
print("temp es: ", placa.temperatura.leer())
print("analogico es: ", placa.analogico.leer())

