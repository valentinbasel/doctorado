#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import domoticaro
import time

###############################################################################
#  Ejemplo de como prender y apagar el RELE 1 de la placa domoticaro
###############################################################################

placa = domoticaro.iniciar("weemos")
placa.iniciar("http://192.168.100.178/")

placa.rele1.on()  # prendemos el rele 1
time.sleep(2)  # una pausa de 1 segundo
placa.rele1.off()  # apagamos el rele 1
time.sleep(2)  # una pausa de 1 segundo

# ahora controlamos el rele 2

placa.rele2.on()  # prendemos el rele 2
time.sleep(2)  # una pausa de 1 segundo
placa.rele2.off()  # apagamos el rele 2
time.sleep(2)  # una pausa de 1 segundo


###############################################################################
