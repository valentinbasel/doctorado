#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from bot.bot import BOT

###############################################################################
#  Ejemplo de como leer texto de un mensaje y efectuar acciones en funcion
#  del texto enviado
###############################################################################

# esta es la clave que geberamos con botfather para poder controlar el bot
token = '1044957463:AAFVOuVzjl1W0jSrGx8YaVLZAOrop0OvIRg'
bot = BOT('telegram', token)  # iniciamos el bot

# repetimos infinitamente (o hasta que apretemos CTRL + C)
# Si hay un mensaje, lo leemos y guardamos en variables
# chat_id : el identificador del remitente del mensaje (quien envio el mensaje)
# texto : El texto enviado por el remitente
while True:
    if bot.hay_mensaje() is True:
        # guardamos el id y el texto
        chat_id, texto = bot.recibir_ultimo_mensaje()
        print("el chat es: ", texto)
        texto = texto.lower()  # tranformamos todo el texto a minusculas
        if texto.find("hola") >= 0:
            print("mensaje: ", texto)
            bot.enviar_texto(chat_id, "¡Hola!, ¿como estas?")
            bot.enviar_texto(chat_id, "puedo enviar varios mensajes a la vez")
        else:
            bot.enviar_texto(chat_id, "no encontre la palabra hola")
            bot.enviar_texto(chat_id, "solo puedo reconocer la palabra hola")

