#!/usr/bin/env python3
# -*- coding: utf-8 -*-


###############################################################################
# ejemplo de una entidad completa que interactua con el hardware domoticaro
# y la API de TELEGRAM
#
# Copyright © 2020 Valentin Basel <valentinbasel@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################

from bot.bot import BOT
from clave import token
from clave import usuario
from os import listdir
from respuestas import *
from personalid import PERSONALIDAD

import time
import domoticaro
import os
import random
import time
import cv2

bot = BOT('telegram', token)
living = domoticaro.iniciar("weemos")
living.iniciar("http://192.168.142.180/")
personalidad = PERSONALIDAD(70, 15, 15)
carpeta = "/home/vbasel/memes"


# una función para tomar una foto con la web cam de la computadora
def tomar_foto():
    camera = cv2.VideoCapture(0)
    for i in range(10):
        return_value, image = camera.read()
    cv2.imwrite('/home/vbasel/foto_casa.png', image)
    del(camera)


def mostrar_temp():
    T = living.temperatura.leer()
    respuesta = "la temperatura actual en el living es: "+str(T)+"°."
    rand_emoji = random.choice(icono_pos)
    rand_resp = random.choice(resp_pos)
    print(rand_emoji)
    respuesta = rand_resp + "\n" + respuesta + "\n" + rand_emoji
    return respuesta


def mostrar_clima():
    T = living.temperatura.leer()
    H = living.humedad.leer()
    resp_T = "la temperatura actual es: " + str(T) + "°\n"
    rand_emoji = random.choice(icono_pos)
    rand_resp = random.choice(resp_pos)
    resp_H = "y la humedad ambiente en el living es: " + str(H) + "%.\n"
    resp_final = rand_resp + "\n" + resp_T + resp_H + "\n" + rand_emoji
    return resp_final


def prender_luz(p):
    rand_emoji = random.choice(icono_pos)
    rand_resp = random.choice(resp_pos)
    luz = living.rele1.estado()
    if p == 1:
        if luz == 1:
            R_luz = "la luz ya esta prendida"
        else:
            R_luz = "enciendo la luz"
        living.rele1.on()
    else:
        if luz == 0:
            R_luz = "la luz ya esta apagada"
        else:
            R_luz = "apago la luz"
        living.rele1.off()
    resp_final = rand_resp + "\n" + R_luz + "\n" + rand_emoji
    return resp_final


while True:
    if bot.hay_mensaje() is True:
        chat_id, texto = bot.recibir_ultimo_mensaje()
        texto = texto.lower()
        print("recibi el texto: ", texto)
        print("enviado por el ID: ", chat_id)
        print("------------------------------")
        estado = personalidad.estado()
        print("mi estado es :", estado)
        tam_tex = len(texto.split(" ")) 
        if tam_tex <= 1:
            bot.enviar_texto(chat_id, "no no, nada de monosilabicos")
            bot.enviar_texto(chat_id, "a mi escribime bien, usando pronombres y todo eso... y se amable!")
        else:
            if estado == "amable":
                if texto.find("temperatura") > -1:
                    R = mostrar_temp()
                    bot.enviar_texto(chat_id, R)

                elif texto.find("foto") > -1:
                    tomar_foto()
                    bot.enviar_imagen(chat_id, "/home/vbasel/foto_casa.png")
                    bot.enviar_texto(chat_id, "asi esta el living en estos momentos")

                elif texto.find("ayuda") > -1:
                    bot.enviar_texto(chat_id,res_ayuda) 
                elif texto.find("clima") > -1:
                    R = mostrar_clima()
                    bot.enviar_texto(chat_id, R)
                elif texto.find("chiste") > -1:
                    R = random.choice(chistes)
                    bot.enviar_texto(chat_id, R)
                    bot.enviar_texto(chat_id, "😅")
                elif texto.find("memes") > -1:
                    R_arch = random.choice(listdir(carpeta))
                    R = carpeta + "/" + R_arch
                    bot.enviar_imagen(chat_id, R)
                elif texto.find("luz") > -1:
                    
                    if texto.find("prende") >-1:
                        p = 1
                    elif texto.find ("apaga") > -1:
                        p = 0
                    else:
                        bot.enviar_texto(chat_id, "prendo o apago la luz??") 
                        bot.enviar_texto(chat_id, "por las dudas apago la luz")
                        p = 0
                    R = prender_luz(p)
                    bot.enviar_texto(chat_id, R)
                else:
                    R = random.choice(resp_no_entiendo)
                    bot.enviar_texto(chat_id, R)
                    bot.enviar_texto(chat_id, "por las dudas te recuerdo que puedes escribir: 'ayuda' para que te cuente que puedo hacer por ti")
            elif estado == "chiste":
                R = random.choice(chistes)
                bot.enviar_texto(chat_id, "mejor te cuento un chiste")
                bot.enviar_texto(chat_id, R)
                bot.enviar_texto(chat_id, "😅")
            else:
                rand_emoji = random.choice(icono_neg)
                rand_resp = random.choice(resp_neg)
                bot.enviar_texto(chat_id, rand_resp)
                bot.enviar_texto(chat_id, "estoy de mal humor")
                bot.enviar_texto(chat_id, rand_emoji)
                

