#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###############################################################################
# variables de texto con respuesats y comentarios
# Copyright © 2020 Valentin Basel <valentinbasel@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################



# algunas respuestas positivas que nuestro DIGI-ente puede hacer
resp_pos = ["ok", "esta bien", "lo hago solo porque quiero", "de una",
            "de unesco",
            "me encata ayudarte", "dale", "okidoki", "entendido", "enterado",
            "si mi general", "siempre listo", "por su pollo", "por su puerto",
            "por su puesto", "a full"
            ]

icono_pos = [
            "😀", "😃", "😄", "😁", "😆", "😅", "😂", "🤣", 
            "😇", "😉", "😊", "🙂", "🙃", "😋", "😍", "🥰", "😘", "😗", "😙", 
            "😚", "🤪", "😜", "😝", "😛", "🤑", "😎" 
            ]
# algunas respuestas negativas genericas
resp_neg = ["ni en pedo", "no", "no se e antoja", "no lo hare",
            "no dijiste las palabras magicas asi que NO", "nones", "minga", 
            "ni ahi", "nunca", "jamas", "no veo porque hacerlo", "nop",
            "no creo", "pedimelo otro dia hoy no tengo ganas"
            ]
icono_neg = [
            "😣", "😖", "😫", "😩", "🥱", "😤", "😮", "😱", "😨", "😰", "😯", 
            "😦", "😧", "😢", "😥", "😪", "😓", "😭", "😵", "🥴", "😲", "🤯", 
            "🤐", "😷", "🤕", "🤮", "🤢", "🤧", "🥵", "🥶", "😴",  "😈", "👿", 
            "👹", "👺", "💩"
            ]
resp_no_entiendo = [ "no entendi", "¿que significa lo que dijiste?",
                     "no te entiendo", "no comprendo", "escribi bien que no entiendo",
                     "¿que trataste de decir?", 
                     "¿podrias repetir lo que dijiste?, no entendi",
                     "perdon, pero no logro entender lo que me pedis", 
                     "si escribis: 'ayuda', pueo decirte algunas cosas",
                     "recuerda, que soy un ser muy limitado, recuerda armar bien la oración para que te entienda",
                     "¿podrias reformular tu pregunta?, porque realmente no entendi",
                     "se mas especifico",
                     "escribi bien, que no comprendo", "por favor, no te entiendo, repetime la pregunta"
                    ]
chistes = [

            """ — ¿Por qué las focas del circo miran siempre hacia arriba?

                — Porque es donde están los focos.
            """,
            """
            — ¡Estás obsesionado con la comida!

            — No sé a que te refieres croquetamente.
            """,
            """
            — ¿Por qué estás hablando con esas zapatillas?

            — Porque pone "converse"
            """,
            """
            — ¿Sabes cómo se queda un mago después de comer?

            — Magordito
            """,
            """
            — Me da un café con leche corto.

            — Se me ha roto la máquina, cambio.
            """,
            """
            — Buenos días, me gustaría alquilar "Batman Forever".

            — No es posible, tiene que devolverla tomorrow.
            """,
            """
            — ¡Camarero! Este filete tiene muchos nervios.

            — Normal, es la primera vez que se lo comen.
            """,
            """
            — ¿Qué le dice un techo a otro?

            — Techo de menos.
            """,
            """
            — Buenos días, quería una camiseta de un personaje inspirador.

            — ¿Ghandi?

            — No, mediani.
            """,
            """
            — Hola, ¿está Agustín?

            — No, estoy incomodín.
            """,
            """
            — ¿Cuál es la fruta más divertida?

            — La naranja ja ja ja ja
            """,
            """
            — ¿Dónde cuelga Superman su supercapa?

            — En superchero
            """
            ]
res_ayuda = """ Mi nombre es icarobot, soy una entidad digital pensada para controlar
la casa y sus distintas partes.
tengo algunas palabras claves que puedes usar:
- ayuda: muestro este mensaje
- memes: muestro un meme grafico
- chiste: cuento un chiste
- prende la luz: prendo la luz
- apaga la luz: apago la luz
- clima: te digo la temperatura y humedad
- temperatura: te muestro la temperatura
- foto: Tomo una fotografia con la camra de la computadora y te la envio por mensaje
"""

