#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###############################################################################
# una clase con la "personalidad" de nuestro digi-ente
# Copyright © 2020 Valentin Basel <valentinbasel@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################
import random
class PERSONALIDAD(object):

    """Docstring for PERSONALIDAD. """

    def __init__(self, amabilidad, chistes, humor):
        """TODO: to be defined.

        :amabilidad: TODO
        :chistes: TODO
        :humor: TODO

        """
        self._amabilidad = amabilidad
        self._chistes = chistes
        self._malhumor = humor


    def estado(self):
        """TODO: Docstring for estado.
        :returns: TODO

        """
        estado = random.randint(0,100)
        if estado < self._amabilidad:
            return "amable"
        elif estado < self._amabilidad+self._chistes:
            return "chiste"
        else:
            return "enojado"

