EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:wemos_mini
LIBS:lacerar_wemos-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L L7805 U3
U 1 1 5DE6FB24
P 5450 3900
F 0 "U3" H 5300 4025 50  0000 C CNN
F 1 "L7805" H 5450 4025 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-220_Horizontal" H 5475 3750 50  0001 L CIN
F 3 "" H 5450 3850 50  0001 C CNN
	1    5450 3900
	1    0    0    -1  
$EndComp
$Comp
L CP C2
U 1 1 5DE6FB25
P 5950 4200
F 0 "C2" H 5975 4300 50  0000 L CNN
F 1 "CP" H 5975 4100 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D8.0mm_P2.50mm" H 5988 4050 50  0001 C CNN
F 3 "" H 5950 4200 50  0001 C CNN
	1    5950 4200
	1    0    0    -1  
$EndComp
$Comp
L CP C1
U 1 1 5DE6FB26
P 4950 4200
F 0 "C1" H 4975 4300 50  0000 L CNN
F 1 "CP" H 4975 4100 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D8.0mm_P2.50mm" H 4988 4050 50  0001 C CNN
F 3 "" H 4950 4200 50  0001 C CNN
	1    4950 4200
	1    0    0    -1  
$EndComp
$Comp
L C C3
U 1 1 5DE6FB27
P 6250 4200
F 0 "C3" H 6275 4300 50  0000 L CNN
F 1 "C" H 6275 4100 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 6288 4050 50  0001 C CNN
F 3 "" H 6250 4200 50  0001 C CNN
	1    6250 4200
	1    0    0    -1  
$EndComp
$Comp
L LED D6
U 1 1 5DE6FB28
P 6600 4050
F 0 "D6" H 6600 4150 50  0000 C CNN
F 1 "LED" H 6600 3950 50  0000 C CNN
F 2 "LEDs:LED_D8.0mm" H 6600 4050 50  0001 C CNN
F 3 "" H 6600 4050 50  0001 C CNN
	1    6600 4050
	0    -1   -1   0   
$EndComp
$Comp
L R R1
U 1 1 5DE6FB29
P 6600 4450
F 0 "R1" V 6700 4450 50  0000 C CNN
F 1 "R" V 6600 4450 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P15.24mm_Horizontal" V 6530 4450 50  0001 C CNN
F 3 "" H 6600 4450 50  0001 C CNN
	1    6600 4450
	1    0    0    -1  
$EndComp
$Comp
L D D5
U 1 1 5DE6FB2A
P 4650 3900
F 0 "D5" H 4650 4000 50  0000 C CNN
F 1 "D" H 4650 3800 50  0000 C CNN
F 2 "Diodes_THT:D_A-405_P7.62mm_Horizontal" H 4650 3900 50  0001 C CNN
F 3 "" H 4650 3900 50  0001 C CNN
	1    4650 3900
	-1   0    0    1   
$EndComp
Wire Wire Line
	4800 3900 5150 3900
Wire Wire Line
	4950 4050 4950 3900
Connection ~ 4950 3900
Wire Wire Line
	5750 3900 6800 3900
Wire Wire Line
	6600 4300 6600 4200
Wire Wire Line
	6250 4050 6250 3900
Connection ~ 6250 3900
Wire Wire Line
	5950 4050 5950 3900
Connection ~ 5950 3900
Wire Wire Line
	4950 4600 6900 4600
Wire Wire Line
	4950 4600 4950 4350
Wire Wire Line
	5950 4350 5950 4600
Connection ~ 5950 4600
Wire Wire Line
	6250 4350 6250 4600
Connection ~ 6250 4600
Wire Wire Line
	5450 4200 5450 4600
Connection ~ 5450 4600
$Comp
L GND #PWR011
U 1 1 5DE6FB2B
P 5650 4700
F 0 "#PWR011" H 5650 4450 50  0001 C CNN
F 1 "GND" H 5650 4550 50  0000 C CNN
F 2 "" H 5650 4700 50  0001 C CNN
F 3 "" H 5650 4700 50  0001 C CNN
	1    5650 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 4700 5650 4600
Connection ~ 5650 4600
Text HLabel 4400 3900 0    60   Input ~ 0
IN
Text HLabel 6800 3900 2    60   Input ~ 0
OUT
Connection ~ 6600 3900
Wire Wire Line
	4400 3900 4500 3900
Text HLabel 6900 4600 2    60   Input ~ 0
GND
Connection ~ 6600 4600
$EndSCHEMATC
