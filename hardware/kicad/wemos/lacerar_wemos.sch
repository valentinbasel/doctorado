EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 2
Title "Proyecto de domótica basada en el esp12f"
Date "2019-11-17"
Rev "0.1"
Comp "Doctorado en Educación en Ciencias Básicas y Tecnología"
Comment1 "FAMAF - UNC "
Comment2 "Co directora: Dra Luciana Benotti"
Comment3 "director: Dr. Diego Letzen"
Comment4 "Docotorando: Lic. Valentín Basel"
$EndDescr
$Comp
L lacerar_wemos-rescue:ULN2003 U2
U 1 1 5DD1DDAE
P 4700 3400
F 0 "U2" H 4700 3925 50  0000 C CNN
F 1 "ULN2003" H 4700 3850 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_Socket_LongPads" H 4750 2750 50  0001 L CNN
F 3 "" H 4800 3300 50  0001 C CNN
	1    4700 3400
	1    0    0    -1  
$EndComp
Text GLabel 5200 3100 2    60   Input ~ 0
REL_1_OUT
Text GLabel 5200 3200 2    60   Input ~ 0
REL_2_OUT
$Comp
L lacerar_wemos-rescue:D D1
U 1 1 5DD1E20E
P 9000 1250
F 0 "D1" H 9000 1350 50  0000 C CNN
F 1 "1n4002" H 9000 1150 50  0000 C CNN
F 2 "Diode_THT:D_A-405_P7.62mm_Horizontal" H 9000 1250 50  0001 C CNN
F 3 "" H 9000 1250 50  0001 C CNN
	1    9000 1250
	0    1    1    0   
$EndComp
Text GLabel 5200 3900 2    60   Input ~ 0
COM
Text GLabel 8200 700  0    60   Input ~ 0
COM
$Comp
L lacerar_wemos-rescue:Conn_01x02 J2
U 1 1 5DD1F8F1
P 3350 1400
F 0 "J2" H 3350 1500 50  0000 C CNN
F 1 "alimentación 7-12V" H 3550 1600 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 3350 1400 50  0001 C CNN
F 3 "" H 3350 1400 50  0001 C CNN
	1    3350 1400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5DD1FE4E
P 4700 4250
F 0 "#PWR01" H 4700 4000 50  0001 C CNN
F 1 "GND" H 4700 4100 50  0000 C CNN
F 2 "" H 4700 4250 50  0001 C CNN
F 3 "" H 4700 4250 50  0001 C CNN
	1    4700 4250
	1    0    0    -1  
$EndComp
$Sheet
S 2050 1400 550  350 
U 5DE6F9B8
F0 "FUENTE_5V" 60
F1 "FUENTE.sch" 60
F2 "IN" I R 2600 1500 60 
F3 "OUT" I L 2050 1500 60 
F4 "GND" I L 2050 1650 60 
$EndSheet
$Comp
L power:GND #PWR05
U 1 1 5DE6FF8C
P 1950 1650
F 0 "#PWR05" H 1950 1400 50  0001 C CNN
F 1 "GND" H 1950 1500 50  0000 C CNN
F 2 "" H 1950 1650 50  0001 C CNN
F 3 "" H 1950 1650 50  0001 C CNN
	1    1950 1650
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR06
U 1 1 5DE6FF93
P 1950 1500
F 0 "#PWR06" H 1950 1350 50  0001 C CNN
F 1 "+5V" H 1950 1640 50  0000 C CNN
F 2 "" H 1950 1500 50  0001 C CNN
F 3 "" H 1950 1500 50  0001 C CNN
	1    1950 1500
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5DE70E5D
P 3000 1400
F 0 "#PWR07" H 3000 1150 50  0001 C CNN
F 1 "GND" H 3000 1250 50  0000 C CNN
F 2 "" H 3000 1400 50  0001 C CNN
F 3 "" H 3000 1400 50  0001 C CNN
	1    3000 1400
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR08
U 1 1 5DE7164E
P 5100 4050
F 0 "#PWR08" H 5100 3900 50  0001 C CNN
F 1 "+5V" H 5100 4190 50  0000 C CNN
F 2 "" H 5100 4050 50  0001 C CNN
F 3 "" H 5100 4050 50  0001 C CNN
	1    5100 4050
	-1   0    0    1   
$EndComp
Text GLabel 3350 5650 3    60   Input ~ 0
A0
$Comp
L power:+3.3V #PWR09
U 1 1 5DE719E9
P 3250 5800
F 0 "#PWR09" H 3250 5650 50  0001 C CNN
F 1 "+3.3V" H 3200 5950 50  0000 C CNN
F 2 "" H 3250 5800 50  0001 C CNN
F 3 "" H 3250 5800 50  0001 C CNN
	1    3250 5800
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR010
U 1 1 5DE71A33
P 3150 5950
F 0 "#PWR010" H 3150 5700 50  0001 C CNN
F 1 "GND" H 3150 5800 50  0000 C CNN
F 2 "" H 3150 5950 50  0001 C CNN
F 3 "" H 3150 5950 50  0001 C CNN
	1    3150 5950
	1    0    0    -1  
$EndComp
$Comp
L lacerar_wemos-rescue:Conn_01x03 J1
U 1 1 5DE71B71
P 3250 5350
F 0 "J1" V 3250 5550 50  0000 C CNN
F 1 "Analogico" V 3350 5350 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 3250 5350 50  0001 C CNN
F 3 "" H 3250 5350 50  0001 C CNN
	1    3250 5350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5100 3900 5200 3900
Wire Wire Line
	5200 3100 5100 3100
Wire Wire Line
	5100 3200 5200 3200
Wire Wire Line
	5200 3300 5100 3300
Wire Wire Line
	5100 3400 5200 3400
Wire Wire Line
	4700 4100 4700 4250
Wire Wire Line
	1950 1500 2050 1500
Wire Wire Line
	1950 1650 2050 1650
Wire Wire Line
	5100 3900 5100 4050
$Comp
L Connector_Generic:Conn_01x02 J3
U 1 1 5E0A2E08
P 10200 1250
F 0 "J3" H 10280 1242 50  0000 L CNN
F 1 "salida_1" H 10300 1150 50  0000 L CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 10200 1250 50  0001 C CNN
F 3 "~" H 10200 1250 50  0001 C CNN
	1    10200 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	10000 1250 10000 950 
Wire Wire Line
	8800 1050 9000 1050
Wire Wire Line
	9000 1050 9000 1100
Wire Wire Line
	9000 1050 9200 1050
Wire Wire Line
	9200 1050 9200 1000
Wire Wire Line
	9200 1000 9350 1000
Connection ~ 9000 1050
Wire Wire Line
	9350 1600 9000 1600
Wire Wire Line
	9000 1600 9000 1500
Connection ~ 9000 1500
Wire Wire Line
	9000 1500 9000 1400
$Comp
L Connector_Generic:Conn_01x02 J4
U 1 1 5E0B12A4
P 10250 3050
F 0 "J4" H 10200 3150 50  0000 L CNN
F 1 "ENTRADA" H 10350 3000 50  0000 L CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 10250 3050 50  0001 C CNN
F 3 "~" H 10250 3050 50  0001 C CNN
	1    10250 3050
	1    0    0    -1  
$EndComp
Text GLabel 9850 3150 0    50   Input ~ 0
vivo
Wire Wire Line
	9850 3050 10050 3050
Wire Wire Line
	10050 3150 9850 3150
Text GLabel 9550 1700 0    50   Input ~ 0
vivo
Text GLabel 9850 3050 0    50   Input ~ 0
masa
Text GLabel 10000 1500 3    50   Input ~ 0
masa
Wire Wire Line
	9550 1700 9750 1700
Wire Wire Line
	9750 1700 9750 1600
Wire Wire Line
	10000 1500 10000 1350
$Comp
L Relay:SANYOU_SRD_Form_C K1
U 1 1 5E222623
P 9550 1300
F 0 "K1" H 9980 1346 50  0000 L CNN
F 1 "RELAY" H 9150 1150 50  0000 L CNN
F 2 "Relay_THT:Relay_SPDT_SANYOU_SRD_Series_Form_C" H 10000 1250 50  0001 L CNN
F 3 "http://www.sanyourelay.ca/public/products/pdf/SRD.pdf" H 9550 1300 50  0001 C CNN
	1    9550 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	9850 1000 9850 950 
Wire Wire Line
	9850 950  10000 950 
Wire Wire Line
	5100 3500 5200 3500
$Comp
L Device:R R2
U 1 1 5E3B7A43
P 8450 900
F 0 "R2" V 8243 900 50  0000 C CNN
F 1 "470" V 8334 900 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 8380 900 50  0001 C CNN
F 3 "~" H 8450 900 50  0001 C CNN
	1    8450 900 
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D2
U 1 1 5E3B8314
P 8450 1300
F 0 "D2" V 8489 1183 50  0000 R CNN
F 1 "LED" V 8398 1183 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 8450 1300 50  0001 C CNN
F 3 "~" H 8450 1300 50  0001 C CNN
	1    8450 1300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8400 1500 8450 1500
Wire Wire Line
	8450 1450 8450 1500
Connection ~ 8450 1500
Wire Wire Line
	8450 1500 9000 1500
Wire Wire Line
	8450 1150 8450 1050
Connection ~ 5100 3900
Text GLabel 4150 3100 0    60   Input ~ 0
REL1
Text GLabel 4150 3200 0    60   Input ~ 0
REL_2
Text GLabel 4150 3400 0    60   Input ~ 0
LED_R
Text GLabel 4150 3300 0    60   Input ~ 0
LED_G
Text GLabel 4150 3500 0    60   Input ~ 0
LED_B
Wire Wire Line
	4150 3100 4300 3100
Wire Wire Line
	4300 3200 4150 3200
Wire Wire Line
	4150 3300 4300 3300
Wire Wire Line
	4300 3400 4150 3400
Wire Wire Line
	4150 3500 4300 3500
Text GLabel 5200 3500 2    60   Input ~ 0
LED_B_OUT
Text GLabel 5200 3300 2    60   Input ~ 0
LED_R_OUT
Text GLabel 5200 3400 2    60   Input ~ 0
LED_G_OUT
Text GLabel 8400 1500 0    60   Input ~ 0
REL_1_OUT
Text GLabel 6000 5500 3    60   Input ~ 0
LED_R_OUT
Text GLabel 5900 5500 3    60   Input ~ 0
LED_G_OUT
Text GLabel 5800 5500 3    60   Input ~ 0
LED_B_OUT
Text GLabel 5700 5500 3    60   Input ~ 0
COM
$Comp
L Connector_Generic:Conn_01x04 J8
U 1 1 5E3CF6C2
P 5800 5300
F 0 "J8" V 5672 5480 50  0000 L CNN
F 1 "conector_RGB" V 5900 5050 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 5800 5300 50  0001 C CNN
F 3 "~" H 5800 5300 50  0001 C CNN
	1    5800 5300
	0    -1   -1   0   
$EndComp
$Comp
L lacerar_wemos-rescue:D D4
U 1 1 5E3D22AA
P 9050 2200
F 0 "D4" H 9050 2300 50  0000 C CNN
F 1 "1n4002" H 9050 2100 50  0000 C CNN
F 2 "Diode_THT:D_A-405_P7.62mm_Horizontal" H 9050 2200 50  0001 C CNN
F 3 "" H 9050 2200 50  0001 C CNN
	1    9050 2200
	0    1    1    0   
$EndComp
Text GLabel 8200 1700 0    60   Input ~ 0
COM
$Comp
L Connector_Generic:Conn_01x02 J9
U 1 1 5E3D22B5
P 10250 2200
F 0 "J9" H 10330 2192 50  0000 L CNN
F 1 "salida_2" H 10350 2100 50  0000 L CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 10250 2200 50  0001 C CNN
F 3 "~" H 10250 2200 50  0001 C CNN
	1    10250 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	10050 2200 10050 1900
Wire Wire Line
	8850 2000 9050 2000
Wire Wire Line
	9050 2000 9050 2050
Wire Wire Line
	9050 2000 9250 2000
Wire Wire Line
	9250 2000 9250 1950
Wire Wire Line
	9250 1950 9400 1950
Connection ~ 9050 2000
Wire Wire Line
	9400 2550 9050 2550
Wire Wire Line
	9050 2550 9050 2450
Connection ~ 9050 2450
Wire Wire Line
	9050 2450 9050 2350
Text GLabel 9600 2650 0    50   Input ~ 0
vivo
Text GLabel 10050 2450 3    50   Input ~ 0
masa
Wire Wire Line
	9600 2650 9800 2650
Wire Wire Line
	9800 2650 9800 2550
Wire Wire Line
	10050 2450 10050 2300
$Comp
L Relay:SANYOU_SRD_Form_C K2
U 1 1 5E3D22CF
P 9600 2250
F 0 "K2" H 10030 2296 50  0000 L CNN
F 1 "RELAY" H 9200 2100 50  0000 L CNN
F 2 "Relay_THT:Relay_SPDT_SANYOU_SRD_Series_Form_C" H 10050 2200 50  0001 L CNN
F 3 "http://www.sanyourelay.ca/public/products/pdf/SRD.pdf" H 9600 2250 50  0001 C CNN
	1    9600 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	9900 1950 9900 1900
Wire Wire Line
	9900 1900 10050 1900
$Comp
L Device:R R3
U 1 1 5E3D22DB
P 8500 1850
F 0 "R3" V 8293 1850 50  0000 C CNN
F 1 "470" V 8384 1850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 8430 1850 50  0001 C CNN
F 3 "~" H 8500 1850 50  0001 C CNN
	1    8500 1850
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D3
U 1 1 5E3D22E5
P 8500 2250
F 0 "D3" V 8539 2133 50  0000 R CNN
F 1 "LED" V 8448 2133 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 8500 2250 50  0001 C CNN
F 3 "~" H 8500 2250 50  0001 C CNN
	1    8500 2250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8450 2450 8500 2450
Wire Wire Line
	8500 2400 8500 2450
Connection ~ 8500 2450
Wire Wire Line
	8500 2450 9050 2450
Wire Wire Line
	8500 2100 8500 2000
Text GLabel 8450 2450 0    60   Input ~ 0
REL_2_OUT
$Comp
L lacerar_wemos-rescue:Conn_01x03 J6
U 1 1 5E3E0FA3
P 4350 5350
F 0 "J6" V 4350 5550 50  0000 C CNN
F 1 "DTH11" V 4450 5350 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 4350 5350 50  0001 C CNN
F 3 "" H 4350 5350 50  0001 C CNN
	1    4350 5350
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J7
U 1 1 5E3E1AB8
P 4850 5350
F 0 "J7" V 4814 5062 50  0000 R CNN
F 1 "pantalla_OLED" V 4950 5500 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 4850 5350 50  0001 C CNN
F 3 "~" H 4850 5350 50  0001 C CNN
	1    4850 5350
	0    -1   -1   0   
$EndComp
Text GLabel 4350 5900 3    50   Input ~ 0
DTH11
$Comp
L power:GND #PWR013
U 1 1 5E3E4963
P 4250 5700
F 0 "#PWR013" H 4250 5450 50  0001 C CNN
F 1 "GND" H 4250 5550 50  0000 C CNN
F 2 "" H 4250 5700 50  0001 C CNN
F 3 "" H 4250 5700 50  0001 C CNN
	1    4250 5700
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR014
U 1 1 5E3E4DE2
P 4450 5700
F 0 "#PWR014" H 4450 5550 50  0001 C CNN
F 1 "+5V" H 4450 5840 50  0000 C CNN
F 2 "" H 4450 5700 50  0001 C CNN
F 3 "" H 4450 5700 50  0001 C CNN
	1    4450 5700
	-1   0    0    1   
$EndComp
Wire Wire Line
	4250 5700 4250 5550
Wire Wire Line
	4450 5700 4450 5550
Wire Wire Line
	4350 5900 4350 5550
$Comp
L power:+5V #PWR016
U 1 1 5E3ECADA
P 4850 5650
F 0 "#PWR016" H 4850 5500 50  0001 C CNN
F 1 "+5V" H 4850 5790 50  0000 C CNN
F 2 "" H 4850 5650 50  0001 C CNN
F 3 "" H 4850 5650 50  0001 C CNN
	1    4850 5650
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR015
U 1 1 5E3ECECA
P 4750 5700
F 0 "#PWR015" H 4750 5450 50  0001 C CNN
F 1 "GND" H 4750 5550 50  0000 C CNN
F 2 "" H 4750 5700 50  0001 C CNN
F 3 "" H 4750 5700 50  0001 C CNN
	1    4750 5700
	1    0    0    -1  
$EndComp
Text GLabel 4950 5850 3    50   Input ~ 0
SCL
Text GLabel 5050 5850 3    50   Input ~ 0
SDA
Wire Wire Line
	4750 5700 4750 5550
Wire Wire Line
	4850 5650 4850 5550
Wire Wire Line
	4950 5850 4950 5550
Wire Wire Line
	5050 5850 5050 5550
$Comp
L Connector_Generic:Conn_01x02 J5
U 1 1 5E3FA242
P 3800 5350
F 0 "J5" V 3800 5450 50  0000 L CNN
F 1 "RX-TX" V 3900 5200 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 3800 5350 50  0001 C CNN
F 3 "~" H 3800 5350 50  0001 C CNN
	1    3800 5350
	0    -1   -1   0   
$EndComp
Text GLabel 3800 5650 3    60   Input ~ 0
RX
Text GLabel 3900 5650 3    60   Input ~ 0
TX
Wire Wire Line
	3900 5650 3900 5550
Wire Wire Line
	3800 5650 3800 5550
Wire Wire Line
	3150 5950 3150 5550
Wire Wire Line
	3250 5800 3250 5550
Wire Wire Line
	3350 5650 3350 5550
$Comp
L MCU_Module:WeMos_D1_mini U1
U 1 1 5E4F12AF
P 2550 3600
F 0 "U1" H 2350 2850 50  0000 C CNN
F 1 "WeMos_D1_mini" H 2900 2850 50  0000 C CNN
F 2 "Module:WEMOS_D1_mini_light" H 2550 2450 50  0001 C CNN
F 3 "https://wiki.wemos.cc/products:d1:d1_mini#documentation" H 700 2450 50  0001 C CNN
	1    2550 3600
	1    0    0    -1  
$EndComp
Text GLabel 3050 3500 2    50   Input ~ 0
DTH11
Text GLabel 3100 3300 2    50   Input ~ 0
SDA
Text GLabel 3100 3400 2    50   Input ~ 0
SCL
Wire Wire Line
	2950 3300 3100 3300
Wire Wire Line
	3100 3400 2950 3400
Wire Wire Line
	2950 3500 3050 3500
Text GLabel 1950 3500 0    60   Input ~ 0
RX
Text GLabel 1950 3600 0    60   Input ~ 0
TX
Wire Wire Line
	1950 3500 2150 3500
Wire Wire Line
	2150 3600 1950 3600
Text GLabel 3050 4000 2    60   Input ~ 0
LED_B
Wire Wire Line
	2950 4000 3050 4000
Text GLabel 3050 3900 2    60   Input ~ 0
LED_R
Wire Wire Line
	3050 3900 2950 3900
Text GLabel 3050 3800 2    60   Input ~ 0
LED_G
Wire Wire Line
	2950 3800 3050 3800
Text GLabel 3050 3700 2    60   Input ~ 0
REL_2
Wire Wire Line
	3050 3700 2950 3700
Text GLabel 3100 3200 2    60   Input ~ 0
REL1
Wire Wire Line
	3100 3200 2950 3200
Text GLabel 3100 3100 2    60   Input ~ 0
A0
Wire Wire Line
	3100 3100 2950 3100
$Comp
L power:GND #PWR04
U 1 1 5E5279C0
P 2550 4500
F 0 "#PWR04" H 2550 4250 50  0001 C CNN
F 1 "GND" H 2550 4350 50  0000 C CNN
F 2 "" H 2550 4500 50  0001 C CNN
F 3 "" H 2550 4500 50  0001 C CNN
	1    2550 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 4500 2550 4400
$Comp
L power:+5V #PWR03
U 1 1 5E52D866
P 2450 2700
F 0 "#PWR03" H 2450 2550 50  0001 C CNN
F 1 "+5V" H 2450 2840 50  0000 C CNN
F 2 "" H 2450 2700 50  0001 C CNN
F 3 "" H 2450 2700 50  0001 C CNN
	1    2450 2700
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR012
U 1 1 5E52E26F
P 2650 2700
F 0 "#PWR012" H 2650 2550 50  0001 C CNN
F 1 "+3.3V" H 2650 2840 50  0000 C CNN
F 2 "" H 2650 2700 50  0001 C CNN
F 3 "" H 2650 2700 50  0001 C CNN
	1    2650 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 2800 2650 2700
Wire Wire Line
	2450 2800 2450 2700
Wire Wire Line
	1650 4250 1650 4150
$Comp
L power:GND #PWR02
U 1 1 5E548D19
P 1650 4250
F 0 "#PWR02" H 1650 4000 50  0001 C CNN
F 1 "GND" H 1650 4100 50  0000 C CNN
F 2 "" H 1650 4250 50  0001 C CNN
F 3 "" H 1650 4250 50  0001 C CNN
	1    1650 4250
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push sw_push1
U 1 1 5E548D23
P 1650 3950
F 0 "sw_push1" V 1604 4098 50  0000 L CNN
F 1 "RESET" V 1695 4098 50  0000 L CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 1650 4150 50  0001 C CNN
F 3 "~" H 1650 4150 50  0001 C CNN
	1    1650 3950
	0    1    1    0   
$EndComp
Wire Wire Line
	2150 3200 1650 3200
Wire Wire Line
	1650 3200 1650 3750
Wire Wire Line
	3000 1400 3150 1400
Wire Wire Line
	2600 1500 3150 1500
Wire Wire Line
	8800 700  8800 1050
Wire Wire Line
	8450 750  8450 700 
Connection ~ 8450 700 
Wire Wire Line
	8450 700  8800 700 
Wire Wire Line
	8500 1700 8850 1700
Wire Wire Line
	8850 1700 8850 2000
Wire Wire Line
	8200 1700 8500 1700
Connection ~ 8500 1700
Wire Wire Line
	8200 700  8450 700 
$Comp
L Mechanical:MountingHole H1
U 1 1 5E64AF24
P 8500 4850
F 0 "H1" H 8600 4896 50  0000 L CNN
F 1 "pin" H 8600 4805 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.1mm" H 8500 4850 50  0001 C CNN
F 3 "~" H 8500 4850 50  0001 C CNN
	1    8500 4850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5E64F4A6
P 8500 5050
F 0 "H2" H 8600 5096 50  0000 L CNN
F 1 "pin" H 8600 5005 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.1mm" H 8500 5050 50  0001 C CNN
F 3 "~" H 8500 5050 50  0001 C CNN
	1    8500 5050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5E64F6A4
P 8500 5250
F 0 "H3" H 8600 5296 50  0000 L CNN
F 1 "pin" H 8600 5205 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.1mm" H 8500 5250 50  0001 C CNN
F 3 "~" H 8500 5250 50  0001 C CNN
	1    8500 5250
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5E64F971
P 8500 5450
F 0 "H4" H 8600 5496 50  0000 L CNN
F 1 "pin" H 8600 5405 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.1mm" H 8500 5450 50  0001 C CNN
F 3 "~" H 8500 5450 50  0001 C CNN
	1    8500 5450
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H5
U 1 1 5E64FB62
P 8500 5700
F 0 "H5" H 8600 5746 50  0000 L CNN
F 1 "pin" H 8600 5655 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.1mm" H 8500 5700 50  0001 C CNN
F 3 "~" H 8500 5700 50  0001 C CNN
	1    8500 5700
	1    0    0    -1  
$EndComp
$EndSCHEMATC
