EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:MCU_Microchip_PIC18
LIBS:lacerar-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L L7805 U2
U 1 1 5CE23476
P 5250 2700
F 0 "U2" H 5100 2825 50  0000 C CNN
F 1 "L7805" H 5250 2825 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-220_Horizontal" H 5275 2550 50  0001 L CIN
F 3 "" H 5250 2650 50  0001 C CNN
	1    5250 2700
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR045
U 1 1 5CE234AC
P 5250 3250
F 0 "#PWR045" H 5250 3000 50  0001 C CNN
F 1 "GND" H 5250 3100 50  0000 C CNN
F 2 "" H 5250 3250 50  0001 C CNN
F 3 "" H 5250 3250 50  0001 C CNN
	1    5250 3250
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02 J18
U 1 1 5CE234C2
P 3250 2900
F 0 "J18" H 3250 3000 50  0000 C CNN
F 1 "+ / -" V 3400 2800 50  0000 C CNN
F 2 "Connectors:bornier2" H 3250 2900 50  0001 C CNN
F 3 "" H 3250 2900 50  0001 C CNN
	1    3250 2900
	-1   0    0    1   
$EndComp
Text HLabel 5850 2700 2    60   Input ~ 0
salida
$Comp
L GND #PWR046
U 1 1 5CE23BE0
P 3450 3250
F 0 "#PWR046" H 3450 3000 50  0001 C CNN
F 1 "GND" H 3450 3100 50  0000 C CNN
F 2 "" H 3450 3250 50  0001 C CNN
F 3 "" H 3450 3250 50  0001 C CNN
	1    3450 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 2700 5850 2700
Wire Wire Line
	5250 3000 5250 3250
$Comp
L C C8
U 1 1 5CE23C0C
P 5800 3000
F 0 "C8" H 5825 3100 50  0000 L CNN
F 1 "0.1uF" V 5800 2600 50  0000 L CNN
F 2 "Capacitors_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm" H 5838 2850 50  0001 C CNN
F 3 "" H 5800 3000 50  0001 C CNN
	1    5800 3000
	1    0    0    -1  
$EndComp
$Comp
L CP C7
U 1 1 5CE23C29
P 5550 3000
F 0 "C7" H 5575 3100 50  0000 L CNN
F 1 "100uF" V 5550 2550 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D4.0mm_P2.00mm" H 5588 2850 50  0001 C CNN
F 3 "" H 5550 3000 50  0001 C CNN
	1    5550 3000
	1    0    0    -1  
$EndComp
$Comp
L CP C6
U 1 1 5CE23C7E
P 4750 3000
F 0 "C6" H 4775 3100 50  0000 L CNN
F 1 "100uF" H 4775 2900 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D4.0mm_P2.00mm" H 4788 2850 50  0001 C CNN
F 3 "" H 4750 3000 50  0001 C CNN
	1    4750 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 2850 4750 2700
Connection ~ 4750 2700
Wire Wire Line
	5550 2850 5550 2700
Wire Wire Line
	5800 2850 5800 2700
Connection ~ 5800 2700
Wire Wire Line
	4750 3150 5800 3150
Connection ~ 5250 3150
Connection ~ 5550 3150
$Comp
L D D3
U 1 1 5CE273B8
P 4350 2700
F 0 "D3" H 4350 2800 50  0000 C CNN
F 1 "1N4007" H 4350 2600 50  0000 C CNN
F 2 "Diodes_THT:D_5W_P12.70mm_Horizontal" H 4350 2700 50  0001 C CNN
F 3 "" H 4350 2700 50  0001 C CNN
	1    4350 2700
	-1   0    0    1   
$EndComp
Wire Wire Line
	4500 2700 4950 2700
$Comp
L SW_DPDT_x2 SW5
U 1 1 5CE2775C
P 3850 2800
F 0 "SW5" H 3850 2970 50  0000 C CNN
F 1 "ON/OFF" H 3850 2600 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 3850 2800 50  0001 C CNN
F 3 "" H 3850 2800 50  0001 C CNN
	1    3850 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 2800 3650 2800
Wire Wire Line
	4050 2700 4200 2700
Wire Wire Line
	3450 2900 3450 3250
$EndSCHEMATC
