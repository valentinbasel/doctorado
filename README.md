## Doctorado en Educación en Ciencias Básicas y Tecnología

### Introducción

Este repositorio es la unificación de varios repositorios GIT distribuidos (en github), con la idea de poder tener todo el desarrollo de software y hardware para el Doctorado en Educación en Ciencias Básicas y Tecnología (UNC) que estoy cursando (2019).

consta de 3 carpetas donde están los tres proyectos que voy a utilizar para mi investigación:

  * API
  * hardware
  * stalker

### API (domoticaro):

las librerías necesarias para controlar el hardware de IoT, la comunicación con TELEGRAM y distintas funciones para manejo de la cámara web de la computadora.

### Hardware

Las distintas versiones de hardware que pueden ser usadas para el proyecto de IoT. Actualmente esta todo montado sobre el hardware WEMOS D1 Mini (ESP-12F) 

### STALKER

El Software especializado para análisis cualitativo (QDA) mediante Keylogger y screencapture.

Se basa en el trabajo de una interface IDE (TORCAZ) que permite a los estudiantes hacer practicas de python mientras el sistema captura las interacciones del teclado y mouse (y un video con el escritorio de la computadora y el microfono de la notebook), para luego ser trabajado con un analizador.

El analizador (Burrhus) permite analizar los "puntos de interés" (PDI) del video, para transcribir las actividades de los estudiantes, y analizarlas con herramientas de analisis cualitativo (RQDA)