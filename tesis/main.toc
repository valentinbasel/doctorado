\babel@toc {spanish}{}
\contentsline {chapter}{\numberline {1}Contextualización del problema de investigación}{7}{chapter.1}%
\contentsline {section}{\numberline {1.1}Antecedentes de la enseñanza de la informática y robótica en argentina}{9}{section.1.1}%
\contentsline {section}{\numberline {1.2}Lenguaje T.I.M.B.A.}{13}{section.1.2}%
\contentsline {section}{\numberline {1.3}PECOS sistemas}{14}{section.1.3}%
\contentsline {section}{\numberline {1.4}La década del noventa}{15}{section.1.4}%
\contentsline {section}{\numberline {1.5}La inclusión digital y el cambio de paradigma del año 2000}{16}{section.1.5}%
\contentsline {section}{\numberline {1.6}El advenimiento de los NAP de Educación Digital, Programación y Robótica}{24}{section.1.6}%
\contentsline {section}{\numberline {1.7}Microcontroladores para robótica educativa}{26}{section.1.7}%
\contentsline {section}{\numberline {1.8}El contexto internacional}{27}{section.1.8}%
\contentsline {chapter}{\numberline {2}decisiones teóricas sobre el aprendizaje}{31}{chapter.2}%
\contentsline {section}{\numberline {2.1}Primera generación de la teoría Constructivista del aprendizaje: la tradición socio-histórica}{32}{section.2.1}%
\contentsline {section}{\numberline {2.2}Los procesos psicológicos superiores}{33}{section.2.2}%
\contentsline {section}{\numberline {2.3}Segunda Generación: A.N. Leontiev}{35}{section.2.3}%
\contentsline {section}{\numberline {2.4}Tercera generación: Yrjö Engeström}{38}{section.2.4}%
\contentsline {section}{\numberline {2.5}El aprendizaje expansivo}{40}{section.2.5}%
\contentsline {section}{\numberline {2.6}La Teoría de la objetivación}{42}{section.2.6}%
\contentsline {section}{\numberline {2.7}Knowing / becoming}{44}{section.2.7}%
\contentsline {section}{\numberline {2.8}Labor conjunta}{46}{section.2.8}%
\contentsline {section}{\numberline {2.9}El estudiante como propietario privado de su conocimiento}{48}{section.2.9}%
\contentsline {section}{\numberline {2.10}Reflexiones finales sobre el capítulo}{49}{section.2.10}%
\contentsline {chapter}{\numberline {3}Decisiones teóricas sobre la didáctica de la programación}{51}{chapter.3}%
\contentsline {section}{\numberline {3.1}Introducción}{51}{section.3.1}%
\contentsline {section}{\numberline {3.2}El lenguaje LOGO}{52}{section.3.2}%
\contentsline {section}{\numberline {3.3}Construccionismo}{54}{section.3.3}%
\contentsline {section}{\numberline {3.4}La importancia de la depuración}{56}{section.3.4}%
\contentsline {section}{\numberline {3.5}El fenómeno QWERTY}{57}{section.3.5}%
\contentsline {section}{\numberline {3.6}El problema de los bienes comunes del conocimiento}{58}{section.3.6}%
\contentsline {section}{\numberline {3.7}Soberanía tecnológica}{60}{section.3.7}%
\contentsline {section}{\numberline {3.8}El software libre y educación}{62}{section.3.8}%
\contentsline {section}{\numberline {3.9}Hardware de especificaciones libres en educación}{70}{section.3.9}%
\contentsline {section}{\numberline {3.10}Robótica educativa}{74}{section.3.10}%
\contentsline {chapter}{\numberline {4}Reflexiones metodológicas}{79}{chapter.4}%
\contentsline {section}{\numberline {4.1}La investigación cualitativa}{79}{section.4.1}%
\contentsline {section}{\numberline {4.2}El estudio de caso}{81}{section.4.2}%
\contentsline {section}{\numberline {4.3}El laboratorio cuasi experimental}{83}{section.4.3}%
\contentsline {section}{\numberline {4.4}Herramientas de investigación cualitativa}{87}{section.4.4}%
\contentsline {section}{\numberline {4.5}Software para recolección de datos (Torcaz)}{88}{section.4.5}%
\contentsline {section}{\numberline {4.6}Software para investigación cualitativa (Saturar)}{92}{section.4.6}%
\contentsline {section}{\numberline {4.7}La entrevista de auto confrontación}{96}{section.4.7}%
\contentsline {section}{\numberline {4.8}Investigación basada en diseño}{98}{section.4.8}%
\contentsline {section}{\numberline {4.9}Primera fase: preparación del diseño}{100}{section.4.9}%
\contentsline {section}{\numberline {4.10}Segunda fase: implementación del diseño}{101}{section.4.10}%
\contentsline {section}{\numberline {4.11}Tercera fase: análisis retrospectivo}{102}{section.4.11}%
\contentsline {section}{\numberline {4.12}Elecciones Técnicas}{102}{section.4.12}%
\contentsline {section}{\numberline {4.13}Domótica Educativa}{103}{section.4.13}%
\contentsline {section}{\numberline {4.14}Hardware}{105}{section.4.14}%
\contentsline {section}{\numberline {4.15}PCB}{108}{section.4.15}%
\contentsline {section}{\numberline {4.16}Lenguaje de programación}{113}{section.4.16}%
\contentsline {section}{\numberline {4.17}Chatbots computacionales}{115}{section.4.17}%
\contentsline {chapter}{\numberline {5}Procesos de aprendizaje con Python y domótica en bibliotecas populares}{119}{chapter.5}%
\contentsline {section}{\numberline {5.1}Bibliotecas populares}{119}{section.5.1}%
\contentsline {section}{\numberline {5.2}Analizando la actividad con datos de Keylogging}{123}{section.5.2}%
\contentsline {section}{\numberline {5.3}Cursos de programación: Día uno, Python- turtle}{129}{section.5.3}%
\contentsline {section}{\numberline {5.4}Día dos, control del hardware}{143}{section.5.4}%
\contentsline {section}{\numberline {5.5}Día Tres, chatbots y Telegram}{151}{section.5.5}%
\contentsline {section}{\numberline {5.6}Día Cuatro, integrando todo}{157}{section.5.6}%
\contentsline {section}{\numberline {5.7}Reflexiones finales}{165}{section.5.7}%
\contentsline {chapter}{\numberline {6}La experiencia de fabricar PCBs con estudiantes de escuelas secundarias}{167}{chapter.6}%
\contentsline {section}{\numberline {6.1}Introducción}{167}{section.6.1}%
\contentsline {section}{\numberline {6.2}Laboratorio cuasi experimental de construcción}{168}{section.6.2}%
\contentsline {section}{\numberline {6.3}Investigación basada en diseño en la escuela IPEM 137}{174}{section.6.3}%
\contentsline {section}{\numberline {6.4}Caracterización del establecimiento}{175}{section.6.4}%
\contentsline {section}{\numberline {6.5}Preparación del diseño}{177}{section.6.5}%
\contentsline {section}{\numberline {6.6}Observación participante en el aula}{177}{section.6.6}%
\contentsline {section}{\numberline {6.7}Implementación del diseño}{184}{section.6.7}%
\contentsline {section}{\numberline {6.8}Fabricar el propio hardware}{185}{section.6.8}%
\contentsline {section}{\numberline {6.9}Proyecto de 'casa inteligente'}{190}{section.6.9}%
\contentsline {section}{\numberline {6.10}Análisis retrospectivo}{191}{section.6.10}%
\contentsline {section}{\numberline {6.11}Complejidades encontradas en el trabajo con domótica}{193}{section.6.11}%
\contentsline {section}{\numberline {6.12}Reflexiónes finales}{196}{section.6.12}%
\contentsline {chapter}{\numberline {7}Conclusiones}{199}{chapter.7}%
\contentsline {section}{\numberline {7.1}El carácter social de la actividad de programar}{200}{section.7.1}%
\contentsline {section}{\numberline {7.2}La enseñanza como un quehacer político}{201}{section.7.2}%
\contentsline {section}{\numberline {7.3}La no neutralidad de las tecnologías}{202}{section.7.3}%
\contentsline {section}{\numberline {7.4}El software y hardware libre en las escuelas}{203}{section.7.4}%
\contentsline {section}{Referencias bibliográficas}{208}{section.7.4}%
\contentsline {chapter}{\numberline {8}Referencias}{209}{chapter.8}%
\contentsline {chapter}{\numberline {A}Entrevista Ivana Guzman [E1]}{227}{appendix.Alph1}%
\contentsline {chapter}{\numberline {B}Entrevista Liliana Robledo[E2]}{235}{appendix.Alph2}%
\contentsline {chapter}{\numberline {C}Jupyter}{243}{appendix.Alph3}%
\contentsline {subsection}{\numberline {C.0.1}Introducción}{243}{subsection.Alph3.0.1}%
\contentsline {chapter}{\numberline {D}Reporte SATURAR}{255}{appendix.Alph4}%
\contentsline {subsection}{\numberline {D.0.1}Código ejercicios}{255}{subsection.Alph4.0.1}%
\contentsline {subsubsection}{ID: \textbf {61}}{255}{section*.76}%
\contentsline {subsubsection}{Reporte}{255}{section*.77}%
\contentsline {subsubsection}{Imagen:}{256}{section*.78}%
\contentsline {subsubsection}{Transcripción:}{256}{section*.79}%
\contentsline {subsubsection}{Memo}{256}{section*.80}%
\contentsline {subsubsection}{Reporte}{256}{section*.81}%
\contentsline {subsubsection}{Imagen:}{257}{section*.82}%
\contentsline {subsubsection}{Transcripción:}{257}{section*.83}%
\contentsline {subsubsection}{Memo}{257}{section*.84}%
\contentsline {subsubsection}{Reporte}{257}{section*.85}%
\contentsline {subsubsection}{Imagen:}{258}{section*.86}%
\contentsline {subsubsection}{Transcripción:}{258}{section*.87}%
\contentsline {subsubsection}{Memo}{258}{section*.88}%
\contentsline {subsubsection}{Reporte}{258}{section*.89}%
\contentsline {subsubsection}{Imagen:}{259}{section*.90}%
\contentsline {subsubsection}{Transcripción:}{259}{section*.91}%
\contentsline {subsubsection}{Memo}{259}{section*.92}%
\contentsline {subsubsection}{Reporte}{259}{section*.93}%
\contentsline {subsubsection}{Imagen:}{260}{section*.94}%
\contentsline {subsubsection}{Transcripción:}{260}{section*.95}%
\contentsline {subsubsection}{Memo}{260}{section*.96}%
\contentsline {subsubsection}{Reporte}{260}{section*.97}%
\contentsline {subsubsection}{Imagen:}{261}{section*.98}%
\contentsline {subsubsection}{Transcripción:}{261}{section*.99}%
\contentsline {subsubsection}{Memo}{261}{section*.100}%
\contentsline {subsubsection}{Reporte}{261}{section*.101}%
\contentsline {subsubsection}{Imagen:}{262}{section*.102}%
\contentsline {subsubsection}{Transcripción:}{262}{section*.103}%
\contentsline {subsubsection}{Memo}{262}{section*.104}%
\contentsline {subsubsection}{Reporte}{262}{section*.105}%
\contentsline {subsubsection}{Imagen:}{263}{section*.106}%
\contentsline {subsubsection}{Transcripción:}{263}{section*.107}%
\contentsline {subsubsection}{Memo}{263}{section*.108}%
\contentsline {subsubsection}{Reporte}{263}{section*.109}%
\contentsline {subsubsection}{Imagen:}{264}{section*.110}%
\contentsline {subsubsection}{Transcripción:}{264}{section*.111}%
\contentsline {subsubsection}{Memo}{264}{section*.112}%
\contentsline {subsubsection}{Reporte}{264}{section*.113}%
\contentsline {subsubsection}{Imagen:}{265}{section*.114}%
\contentsline {subsubsection}{Transcripción:}{265}{section*.115}%
\contentsline {subsubsection}{Memo}{265}{section*.116}%
\contentsline {subsubsection}{Reporte}{265}{section*.117}%
\contentsline {subsubsection}{Imagen:}{266}{section*.118}%
\contentsline {subsubsection}{Transcripción:}{266}{section*.119}%
\contentsline {subsubsection}{Memo}{266}{section*.120}%
\contentsline {subsubsection}{Reporte}{266}{section*.121}%
\contentsline {subsubsection}{Imagen:}{267}{section*.122}%
\contentsline {subsubsection}{Transcripción:}{267}{section*.123}%
\contentsline {subsubsection}{Memo}{267}{section*.124}%
\contentsline {subsubsection}{Reporte}{267}{section*.125}%
\contentsline {subsubsection}{Imagen:}{268}{section*.126}%
\contentsline {subsubsection}{Transcripción:}{268}{section*.127}%
\contentsline {subsubsection}{Memo}{268}{section*.128}%
\contentsline {subsubsection}{Reporte}{268}{section*.129}%
\contentsline {subsubsection}{Imagen:}{269}{section*.130}%
\contentsline {subsubsection}{Transcripción:}{269}{section*.131}%
\contentsline {subsubsection}{Memo}{269}{section*.132}%
\contentsline {subsubsection}{Reporte}{269}{section*.133}%
\contentsline {subsubsection}{Imagen:}{270}{section*.134}%
\contentsline {subsubsection}{Transcripción:}{270}{section*.135}%
\contentsline {subsubsection}{Memo}{270}{section*.136}%
\contentsline {subsubsection}{Reporte}{270}{section*.137}%
\contentsline {subsubsection}{Imagen:}{271}{section*.138}%
\contentsline {subsubsection}{Transcripción:}{271}{section*.139}%
\contentsline {subsubsection}{Memo}{271}{section*.140}%
\contentsline {subsubsection}{Reporte}{271}{section*.141}%
\contentsline {subsubsection}{Imagen:}{272}{section*.142}%
\contentsline {subsubsection}{Transcripción:}{272}{section*.143}%
\contentsline {subsubsection}{Memo}{272}{section*.144}%
\contentsline {subsubsection}{Reporte}{272}{section*.145}%
\contentsline {subsubsection}{Imagen:}{273}{section*.146}%
\contentsline {subsubsection}{Transcripción:}{273}{section*.147}%
\contentsline {subsubsection}{Memo}{273}{section*.148}%
\contentsline {subsubsection}{Reporte}{273}{section*.149}%
\contentsline {subsubsection}{Imagen:}{274}{section*.150}%
\contentsline {subsubsection}{Transcripción:}{274}{section*.151}%
\contentsline {subsubsection}{Memo}{274}{section*.152}%
\contentsline {subsubsection}{Reporte}{274}{section*.153}%
\contentsline {subsubsection}{Imagen:}{275}{section*.154}%
\contentsline {subsubsection}{Transcripción:}{275}{section*.155}%
\contentsline {subsubsection}{Memo}{275}{section*.156}%
\contentsline {subsubsection}{Reporte}{275}{section*.157}%
\contentsline {subsubsection}{Imagen:}{276}{section*.158}%
\contentsline {subsubsection}{Transcripción:}{276}{section*.159}%
\contentsline {subsubsection}{Memo}{276}{section*.160}%
\contentsline {subsubsection}{Reporte}{276}{section*.161}%
\contentsline {subsubsection}{Imagen:}{277}{section*.162}%
\contentsline {subsubsection}{Transcripción:}{277}{section*.163}%
\contentsline {subsubsection}{Memo}{277}{section*.164}%
\contentsline {subsubsection}{Reporte}{277}{section*.165}%
\contentsline {subsubsection}{Imagen:}{278}{section*.166}%
\contentsline {subsubsection}{Transcripción:}{278}{section*.167}%
\contentsline {subsubsection}{Memo}{278}{section*.168}%
\contentsline {subsubsection}{Reporte}{278}{section*.169}%
\contentsline {subsubsection}{Imagen:}{279}{section*.170}%
\contentsline {subsubsection}{Transcripción:}{279}{section*.171}%
\contentsline {subsubsection}{Memo}{279}{section*.172}%
\contentsline {subsubsection}{Reporte}{279}{section*.173}%
\contentsline {subsubsection}{Imagen:}{280}{section*.174}%
\contentsline {subsubsection}{Transcripción:}{280}{section*.175}%
\contentsline {subsubsection}{Memo}{280}{section*.176}%
\contentsline {subsubsection}{Reporte}{280}{section*.177}%
\contentsline {subsubsection}{Imagen:}{281}{section*.178}%
\contentsline {subsubsection}{Transcripción:}{281}{section*.179}%
\contentsline {subsubsection}{Memo}{281}{section*.180}%
\contentsline {subsubsection}{Reporte}{281}{section*.181}%
\contentsline {subsubsection}{Imagen:}{282}{section*.182}%
\contentsline {subsubsection}{Transcripción:}{282}{section*.183}%
\contentsline {subsubsection}{Memo}{282}{section*.184}%
\contentsline {subsubsection}{Reporte}{282}{section*.185}%
\contentsline {subsubsection}{Imagen:}{283}{section*.186}%
\contentsline {subsubsection}{Transcripción:}{283}{section*.187}%
\contentsline {subsubsection}{Memo}{283}{section*.188}%
\contentsline {subsubsection}{Reporte}{283}{section*.189}%
\contentsline {subsubsection}{Imagen:}{284}{section*.190}%
\contentsline {subsubsection}{Transcripción:}{284}{section*.191}%
\contentsline {subsubsection}{Memo}{284}{section*.192}%
\contentsline {subsubsection}{Reporte}{284}{section*.193}%
\contentsline {subsubsection}{Imagen:}{285}{section*.194}%
\contentsline {subsubsection}{Transcripción:}{285}{section*.195}%
\contentsline {subsubsection}{Memo}{285}{section*.196}%
\contentsline {subsubsection}{Reporte}{285}{section*.197}%
\contentsline {subsubsection}{Imagen:}{286}{section*.198}%
\contentsline {subsubsection}{Transcripción:}{286}{section*.199}%
\contentsline {subsubsection}{Memo}{286}{section*.200}%
\contentsline {subsubsection}{Reporte}{286}{section*.201}%
\contentsline {subsubsection}{Imagen:}{287}{section*.202}%
\contentsline {subsubsection}{Transcripción:}{287}{section*.203}%
\contentsline {subsubsection}{Memo}{287}{section*.204}%
\contentsline {subsubsection}{Reporte}{287}{section*.205}%
\contentsline {subsubsection}{Imagen:}{288}{section*.206}%
\contentsline {subsubsection}{Transcripción:}{288}{section*.207}%
\contentsline {subsubsection}{Memo}{288}{section*.208}%
\contentsline {subsubsection}{Reporte}{288}{section*.209}%
\contentsline {subsubsection}{Imagen:}{289}{section*.210}%
\contentsline {subsubsection}{Transcripción:}{289}{section*.211}%
\contentsline {subsubsection}{Memo}{289}{section*.212}%
\contentsline {subsubsection}{Reporte}{289}{section*.213}%
\contentsline {subsubsection}{Imagen:}{290}{section*.214}%
\contentsline {subsubsection}{Transcripción:}{290}{section*.215}%
\contentsline {subsubsection}{Memo}{290}{section*.216}%
\contentsline {subsubsection}{Reporte}{290}{section*.217}%
\contentsline {subsubsection}{Imagen:}{291}{section*.218}%
\contentsline {subsubsection}{Transcripción:}{291}{section*.219}%
\contentsline {subsubsection}{Memo}{291}{section*.220}%
\contentsline {subsubsection}{Reporte}{291}{section*.221}%
\contentsline {subsubsection}{Imagen:}{292}{section*.222}%
\contentsline {subsubsection}{Transcripción:}{292}{section*.223}%
\contentsline {subsubsection}{Memo}{292}{section*.224}%
\contentsline {subsubsection}{Reporte}{292}{section*.225}%
\contentsline {subsubsection}{Imagen:}{293}{section*.226}%
\contentsline {subsubsection}{Transcripción:}{293}{section*.227}%
\contentsline {subsubsection}{Memo}{293}{section*.228}%
\contentsline {subsubsection}{Reporte}{293}{section*.229}%
\contentsline {subsubsection}{Imagen:}{294}{section*.230}%
\contentsline {subsubsection}{Transcripción:}{294}{section*.231}%
\contentsline {subsubsection}{Memo}{294}{section*.232}%
\contentsline {subsubsection}{Reporte}{294}{section*.233}%
\contentsline {subsubsection}{Imagen:}{295}{section*.234}%
\contentsline {subsubsection}{Transcripción:}{295}{section*.235}%
\contentsline {subsubsection}{Memo}{295}{section*.236}%
\contentsline {subsubsection}{Reporte}{295}{section*.237}%
\contentsline {subsubsection}{Imagen:}{296}{section*.238}%
\contentsline {subsubsection}{Transcripción:}{296}{section*.239}%
\contentsline {subsubsection}{Memo}{296}{section*.240}%
\contentsline {subsubsection}{Reporte}{296}{section*.241}%
\contentsline {subsubsection}{Imagen:}{297}{section*.242}%
\contentsline {subsubsection}{Transcripción:}{297}{section*.243}%
\contentsline {subsubsection}{Memo}{297}{section*.244}%
\contentsline {subsubsection}{Reporte}{297}{section*.245}%
\contentsline {subsubsection}{Imagen:}{298}{section*.246}%
\contentsline {subsubsection}{Transcripción:}{298}{section*.247}%
\contentsline {subsubsection}{Memo}{298}{section*.248}%
\contentsline {subsubsection}{Reporte}{298}{section*.249}%
\contentsline {subsubsection}{Imagen:}{299}{section*.250}%
\contentsline {subsubsection}{Transcripción:}{299}{section*.251}%
\contentsline {subsubsection}{Memo}{299}{section*.252}%
\contentsline {subsubsection}{Reporte}{299}{section*.253}%
\contentsline {subsubsection}{Imagen:}{300}{section*.254}%
\contentsline {subsubsection}{Transcripción:}{300}{section*.255}%
\contentsline {subsubsection}{Memo}{300}{section*.256}%
\contentsline {subsubsection}{Reporte}{300}{section*.257}%
\contentsline {subsubsection}{Imagen:}{301}{section*.258}%
\contentsline {subsubsection}{Transcripción:}{301}{section*.259}%
\contentsline {subsubsection}{Memo}{301}{section*.260}%
\contentsline {subsubsection}{Reporte}{301}{section*.261}%
\contentsline {subsubsection}{Imagen:}{302}{section*.262}%
\contentsline {subsubsection}{Transcripción:}{302}{section*.263}%
\contentsline {subsubsection}{Memo}{302}{section*.264}%
\contentsline {subsubsection}{Reporte}{302}{section*.265}%
\contentsline {subsubsection}{Imagen:}{303}{section*.266}%
\contentsline {subsubsection}{Transcripción:}{303}{section*.267}%
\contentsline {subsubsection}{Memo}{303}{section*.268}%
\contentsline {subsubsection}{Reporte}{303}{section*.269}%
\contentsline {subsubsection}{Imagen:}{304}{section*.270}%
\contentsline {subsubsection}{Transcripción:}{304}{section*.271}%
\contentsline {subsubsection}{Memo}{304}{section*.272}%
\contentsline {subsubsection}{Reporte}{304}{section*.273}%
\contentsline {subsubsection}{Imagen:}{305}{section*.274}%
\contentsline {subsubsection}{Transcripción:}{305}{section*.275}%
\contentsline {subsubsection}{Memo}{305}{section*.276}%
\contentsline {subsubsection}{Reporte}{305}{section*.277}%
\contentsline {subsubsection}{Imagen:}{306}{section*.278}%
\contentsline {subsubsection}{Transcripción:}{306}{section*.279}%
\contentsline {subsubsection}{Memo}{306}{section*.280}%
\contentsline {subsubsection}{Reporte}{306}{section*.281}%
\contentsline {subsubsection}{Imagen:}{307}{section*.282}%
\contentsline {subsubsection}{Transcripción:}{307}{section*.283}%
\contentsline {subsubsection}{Memo}{307}{section*.284}%
\contentsline {subsubsection}{Reporte}{307}{section*.285}%
\contentsline {subsubsection}{Imagen:}{308}{section*.286}%
\contentsline {subsubsection}{Transcripción:}{308}{section*.287}%
\contentsline {subsubsection}{Memo}{308}{section*.288}%
\contentsline {subsubsection}{Reporte}{308}{section*.289}%
\contentsline {subsubsection}{Imagen:}{309}{section*.290}%
\contentsline {subsubsection}{Transcripción:}{309}{section*.291}%
\contentsline {subsubsection}{Memo}{309}{section*.292}%
\contentsline {subsubsection}{Reporte}{309}{section*.293}%
\contentsline {subsubsection}{Imagen:}{310}{section*.294}%
\contentsline {subsubsection}{Transcripción:}{310}{section*.295}%
\contentsline {subsubsection}{Memo}{310}{section*.296}%
\contentsline {subsubsection}{Reporte}{310}{section*.297}%
\contentsline {subsubsection}{Imagen:}{311}{section*.298}%
\contentsline {subsubsection}{Transcripción:}{311}{section*.299}%
\contentsline {subsubsection}{Memo}{311}{section*.300}%
\contentsline {subsubsection}{Reporte}{311}{section*.301}%
\contentsline {subsubsection}{Imagen:}{312}{section*.302}%
\contentsline {subsubsection}{Transcripción:}{312}{section*.303}%
\contentsline {subsubsection}{Memo}{312}{section*.304}%
\contentsline {subsubsection}{Reporte}{312}{section*.305}%
\contentsline {subsubsection}{Imagen:}{313}{section*.306}%
\contentsline {subsubsection}{Transcripción:}{313}{section*.307}%
\contentsline {subsubsection}{Memo}{313}{section*.308}%
\contentsline {subsubsection}{Reporte}{313}{section*.309}%
\contentsline {subsubsection}{Imagen:}{314}{section*.310}%
\contentsline {subsubsection}{Transcripción:}{314}{section*.311}%
\contentsline {subsubsection}{Memo}{314}{section*.312}%
\contentsline {subsubsection}{Reporte}{314}{section*.313}%
\contentsline {subsubsection}{Imagen:}{315}{section*.314}%
\contentsline {subsubsection}{Transcripción:}{315}{section*.315}%
\contentsline {subsubsection}{Memo}{315}{section*.316}%
\contentsline {subsubsection}{Reporte}{315}{section*.317}%
\contentsline {subsubsection}{Imagen:}{316}{section*.318}%
\contentsline {subsubsection}{Transcripción:}{316}{section*.319}%
\contentsline {subsubsection}{Memo}{316}{section*.320}%
\contentsline {subsubsection}{Reporte}{316}{section*.321}%
\contentsline {subsubsection}{Imagen:}{317}{section*.322}%
\contentsline {subsubsection}{Transcripción:}{317}{section*.323}%
\contentsline {subsubsection}{Memo}{317}{section*.324}%
\contentsline {subsubsection}{Reporte}{317}{section*.325}%
\contentsline {subsubsection}{Imagen:}{318}{section*.326}%
\contentsline {subsubsection}{Transcripción:}{318}{section*.327}%
\contentsline {subsubsection}{Memo}{318}{section*.328}%
\contentsline {subsubsection}{Reporte}{318}{section*.329}%
\contentsline {subsubsection}{Imagen:}{319}{section*.330}%
\contentsline {subsubsection}{Transcripción:}{319}{section*.331}%
\contentsline {subsubsection}{Memo}{319}{section*.332}%
\contentsline {subsubsection}{Reporte}{319}{section*.333}%
\contentsline {subsubsection}{Imagen:}{320}{section*.334}%
\contentsline {subsubsection}{Transcripción:}{320}{section*.335}%
\contentsline {subsubsection}{Memo}{320}{section*.336}%
\contentsline {subsubsection}{Reporte}{320}{section*.337}%
\contentsline {subsubsection}{Imagen:}{321}{section*.338}%
\contentsline {subsubsection}{Transcripción:}{321}{section*.339}%
\contentsline {subsubsection}{Memo}{321}{section*.340}%
\contentsline {subsubsection}{Reporte}{321}{section*.341}%
\contentsline {subsubsection}{Imagen:}{322}{section*.342}%
\contentsline {subsubsection}{Transcripción:}{322}{section*.343}%
\contentsline {subsubsection}{Memo}{322}{section*.344}%
\contentsline {subsubsection}{Reporte}{322}{section*.345}%
\contentsline {subsubsection}{Imagen:}{323}{section*.346}%
\contentsline {subsubsection}{Transcripción:}{323}{section*.347}%
\contentsline {subsubsection}{Memo}{323}{section*.348}%
\contentsline {subsubsection}{Reporte}{323}{section*.349}%
\contentsline {subsubsection}{Imagen:}{324}{section*.350}%
\contentsline {subsubsection}{Transcripción:}{324}{section*.351}%
\contentsline {subsubsection}{Memo}{324}{section*.352}%
\contentsline {subsubsection}{Reporte}{324}{section*.353}%
\contentsline {subsubsection}{Imagen:}{325}{section*.354}%
\contentsline {subsubsection}{Transcripción:}{325}{section*.355}%
\contentsline {subsubsection}{Memo}{325}{section*.356}%
\contentsline {subsubsection}{Reporte}{325}{section*.357}%
\contentsline {subsubsection}{Imagen:}{326}{section*.358}%
\contentsline {subsubsection}{Transcripción:}{326}{section*.359}%
\contentsline {subsubsection}{Memo}{326}{section*.360}%
\contentsline {subsubsection}{Reporte}{326}{section*.361}%
\contentsline {subsubsection}{Imagen:}{327}{section*.362}%
\contentsline {subsubsection}{Transcripción:}{327}{section*.363}%
\contentsline {subsubsection}{Memo}{327}{section*.364}%
\contentsline {subsubsection}{Reporte}{327}{section*.365}%
\contentsline {subsubsection}{Imagen:}{328}{section*.366}%
\contentsline {subsubsection}{Transcripción:}{328}{section*.367}%
\contentsline {subsubsection}{Memo}{328}{section*.368}%
\contentsline {subsubsection}{Reporte}{328}{section*.369}%
\contentsline {subsubsection}{Imagen:}{329}{section*.370}%
\contentsline {subsubsection}{Transcripción:}{329}{section*.371}%
\contentsline {subsubsection}{Memo}{329}{section*.372}%
\contentsline {subsubsection}{Reporte}{329}{section*.373}%
\contentsline {subsubsection}{Imagen:}{330}{section*.374}%
\contentsline {subsubsection}{Transcripción:}{330}{section*.375}%
\contentsline {subsubsection}{Memo}{330}{section*.376}%
\contentsline {subsubsection}{Reporte}{330}{section*.377}%
\contentsline {subsubsection}{Imagen:}{331}{section*.378}%
\contentsline {subsubsection}{Transcripción:}{331}{section*.379}%
\contentsline {subsubsection}{Memo}{331}{section*.380}%
\contentsline {subsubsection}{Reporte}{331}{section*.381}%
\contentsline {subsubsection}{Imagen:}{332}{section*.382}%
\contentsline {subsubsection}{Transcripción:}{332}{section*.383}%
\contentsline {subsubsection}{Memo}{332}{section*.384}%
\contentsline {subsubsection}{Reporte}{332}{section*.385}%
\contentsline {subsubsection}{Imagen:}{333}{section*.386}%
\contentsline {subsubsection}{Transcripción:}{333}{section*.387}%
\contentsline {subsubsection}{Memo}{333}{section*.388}%
\contentsline {subsubsection}{Reporte}{333}{section*.389}%
\contentsline {subsubsection}{Imagen:}{334}{section*.390}%
\contentsline {subsubsection}{Transcripción:}{334}{section*.391}%
\contentsline {subsubsection}{Memo}{334}{section*.392}%
\contentsline {subsubsection}{Reporte}{334}{section*.393}%
\contentsline {subsubsection}{Imagen:}{334}{section*.394}%
\contentsline {subsubsection}{Transcripción:}{335}{section*.395}%
\contentsline {subsubsection}{Memo}{335}{section*.396}%
\contentsline {subsubsection}{Reporte}{335}{section*.397}%
\contentsline {subsubsection}{Imagen:}{335}{section*.398}%
\contentsline {subsubsection}{Transcripción:}{336}{section*.399}%
\contentsline {subsubsection}{Memo}{336}{section*.400}%
\contentsline {subsubsection}{Reporte}{336}{section*.401}%
\contentsline {subsubsection}{Imagen:}{336}{section*.402}%
\contentsline {subsubsection}{Transcripción:}{337}{section*.403}%
\contentsline {subsubsection}{Memo}{337}{section*.404}%
\contentsline {subsubsection}{Reporte}{337}{section*.405}%
\contentsline {subsubsection}{Imagen:}{337}{section*.406}%
\contentsline {subsubsection}{Transcripción:}{338}{section*.407}%
\contentsline {subsubsection}{Memo}{338}{section*.408}%
\contentsline {subsubsection}{Reporte}{338}{section*.409}%
\contentsline {subsubsection}{Imagen:}{338}{section*.410}%
\contentsline {subsubsection}{Transcripción:}{339}{section*.411}%
\contentsline {subsubsection}{Memo}{339}{section*.412}%
\contentsline {subsubsection}{Reporte}{339}{section*.413}%
\contentsline {subsubsection}{Imagen:}{339}{section*.414}%
\contentsline {subsubsection}{Transcripción:}{340}{section*.415}%
\contentsline {subsubsection}{Memo}{340}{section*.416}%
\contentsline {subsubsection}{Reporte}{340}{section*.417}%
\contentsline {subsubsection}{Imagen:}{340}{section*.418}%
\contentsline {subsubsection}{Transcripción:}{341}{section*.419}%
\contentsline {subsubsection}{Memo}{341}{section*.420}%
\contentsline {subsubsection}{Reporte}{341}{section*.421}%
\contentsline {subsubsection}{Imagen:}{341}{section*.422}%
\contentsline {subsubsection}{Transcripción:}{342}{section*.423}%
\contentsline {subsubsection}{Memo}{342}{section*.424}%
\contentsline {subsubsection}{Reporte}{342}{section*.425}%
\contentsline {subsubsection}{Imagen:}{342}{section*.426}%
\contentsline {subsubsection}{Transcripción:}{343}{section*.427}%
\contentsline {subsubsection}{Memo}{343}{section*.428}%
\contentsline {subsubsection}{Reporte}{343}{section*.429}%
\contentsline {subsubsection}{Imagen:}{343}{section*.430}%
\contentsline {subsubsection}{Transcripción:}{344}{section*.431}%
\contentsline {subsubsection}{Memo}{344}{section*.432}%
\contentsline {subsubsection}{Reporte}{344}{section*.433}%
\contentsline {subsubsection}{Imagen:}{344}{section*.434}%
\contentsline {subsubsection}{Transcripción:}{345}{section*.435}%
\contentsline {subsubsection}{Memo}{345}{section*.436}%
\contentsline {subsubsection}{Reporte}{345}{section*.437}%
\contentsline {subsubsection}{Imagen:}{345}{section*.438}%
\contentsline {subsubsection}{Transcripción:}{346}{section*.439}%
\contentsline {subsubsection}{Memo}{346}{section*.440}%
\contentsline {subsubsection}{Reporte}{346}{section*.441}%
\contentsline {subsubsection}{Imagen:}{346}{section*.442}%
\contentsline {subsubsection}{Transcripción:}{347}{section*.443}%
\contentsline {subsubsection}{Memo}{347}{section*.444}%
\contentsline {subsubsection}{Reporte}{347}{section*.445}%
\contentsline {subsubsection}{Imagen:}{347}{section*.446}%
\contentsline {subsubsection}{Transcripción:}{348}{section*.447}%
\contentsline {subsubsection}{Memo}{348}{section*.448}%
\contentsline {subsubsection}{Reporte}{348}{section*.449}%
\contentsline {subsubsection}{Imagen:}{348}{section*.450}%
\contentsline {subsubsection}{Transcripción:}{349}{section*.451}%
\contentsline {subsubsection}{Memo}{349}{section*.452}%
\contentsline {subsubsection}{Reporte}{349}{section*.453}%
\contentsline {subsubsection}{Imagen:}{350}{section*.454}%
\contentsline {subsubsection}{Transcripción:}{351}{section*.455}%
\contentsline {subsubsection}{Memo}{351}{section*.456}%
\contentsline {subsubsection}{Reporte}{351}{section*.457}%
\contentsline {subsubsection}{Imagen:}{352}{section*.458}%
\contentsline {subsubsection}{Transcripción:}{353}{section*.459}%
\contentsline {subsubsection}{Memo}{353}{section*.460}%
\contentsline {subsubsection}{Reporte}{353}{section*.461}%
\contentsline {subsubsection}{Imagen:}{353}{section*.462}%
\contentsline {subsubsection}{Transcripción:}{354}{section*.463}%
\contentsline {subsubsection}{Memo}{354}{section*.464}%
\contentsline {subsubsection}{Reporte}{354}{section*.465}%
\contentsline {subsubsection}{Imagen:}{354}{section*.466}%
\contentsline {subsubsection}{Transcripción:}{355}{section*.467}%
\contentsline {subsubsection}{Memo}{355}{section*.468}%
\contentsline {subsubsection}{Reporte}{355}{section*.469}%
\contentsline {subsubsection}{Imagen:}{355}{section*.470}%
\contentsline {subsubsection}{Transcripción:}{356}{section*.471}%
\contentsline {subsubsection}{Memo}{356}{section*.472}%
\contentsline {subsubsection}{Reporte}{356}{section*.473}%
\contentsline {subsubsection}{Imagen:}{356}{section*.474}%
\contentsline {subsubsection}{Transcripción:}{357}{section*.475}%
\contentsline {subsubsection}{Memo}{357}{section*.476}%
\contentsline {subsubsection}{Reporte}{357}{section*.477}%
\contentsline {subsubsection}{Imagen:}{357}{section*.478}%
\contentsline {subsubsection}{Transcripción:}{358}{section*.479}%
\contentsline {subsubsection}{Memo}{358}{section*.480}%
\contentsline {subsubsection}{Reporte}{358}{section*.481}%
\contentsline {subsubsection}{Imagen:}{358}{section*.482}%
\contentsline {subsubsection}{Transcripción:}{359}{section*.483}%
\contentsline {subsubsection}{Memo}{359}{section*.484}%
\contentsline {subsubsection}{Reporte}{359}{section*.485}%
\contentsline {subsubsection}{Imagen:}{359}{section*.486}%
\contentsline {subsubsection}{Transcripción:}{360}{section*.487}%
\contentsline {subsubsection}{Memo}{360}{section*.488}%
\contentsline {subsubsection}{Reporte}{360}{section*.489}%
\contentsline {subsubsection}{Imagen:}{360}{section*.490}%
\contentsline {subsubsection}{Transcripción:}{361}{section*.491}%
\contentsline {subsubsection}{Memo}{361}{section*.492}%
\contentsline {subsubsection}{Reporte}{361}{section*.493}%
\contentsline {subsubsection}{Imagen:}{361}{section*.494}%
\contentsline {subsubsection}{Transcripción:}{362}{section*.495}%
\contentsline {subsubsection}{Memo}{362}{section*.496}%
\contentsline {subsubsection}{Reporte}{362}{section*.497}%
\contentsline {subsubsection}{Imagen:}{362}{section*.498}%
\contentsline {subsubsection}{Transcripción:}{363}{section*.499}%
\contentsline {subsubsection}{Memo}{363}{section*.500}%
\contentsline {subsubsection}{Reporte}{363}{section*.501}%
\contentsline {subsubsection}{Imagen:}{363}{section*.502}%
\contentsline {subsubsection}{Transcripción:}{364}{section*.503}%
\contentsline {subsubsection}{Memo}{364}{section*.504}%
\contentsline {subsubsection}{Reporte}{364}{section*.505}%
\contentsline {subsubsection}{Imagen:}{364}{section*.506}%
\contentsline {subsubsection}{Transcripción:}{365}{section*.507}%
\contentsline {subsubsection}{Memo}{365}{section*.508}%
\contentsline {subsubsection}{Reporte}{365}{section*.509}%
\contentsline {subsubsection}{Imagen:}{365}{section*.510}%
\contentsline {subsubsection}{Transcripción:}{366}{section*.511}%
\contentsline {subsubsection}{Memo}{366}{section*.512}%
\contentsline {subsubsection}{Reporte}{366}{section*.513}%
\contentsline {subsubsection}{Imagen:}{366}{section*.514}%
\contentsline {subsubsection}{Transcripción:}{367}{section*.515}%
\contentsline {subsubsection}{Memo}{367}{section*.516}%
\contentsline {subsubsection}{Reporte}{367}{section*.517}%
\contentsline {subsubsection}{Imagen:}{367}{section*.518}%
\contentsline {subsubsection}{Transcripción:}{368}{section*.519}%
\contentsline {subsubsection}{Memo}{368}{section*.520}%
\contentsline {subsubsection}{Reporte}{368}{section*.521}%
\contentsline {subsubsection}{Imagen:}{368}{section*.522}%
\contentsline {subsubsection}{Transcripción:}{369}{section*.523}%
\contentsline {subsubsection}{Memo}{369}{section*.524}%
\contentsline {subsubsection}{Reporte}{369}{section*.525}%
\contentsline {subsubsection}{Imagen:}{369}{section*.526}%
\contentsline {subsubsection}{Transcripción:}{370}{section*.527}%
\contentsline {subsubsection}{Memo}{370}{section*.528}%
\contentsline {subsubsection}{Reporte}{370}{section*.529}%
\contentsline {subsubsection}{Imagen:}{370}{section*.530}%
\contentsline {subsubsection}{Transcripción:}{371}{section*.531}%
\contentsline {subsubsection}{Memo}{371}{section*.532}%
\contentsline {subsubsection}{Reporte}{371}{section*.533}%
\contentsline {subsubsection}{Imagen:}{371}{section*.534}%
\contentsline {subsubsection}{Transcripción:}{372}{section*.535}%
\contentsline {subsubsection}{Memo}{372}{section*.536}%
\contentsline {subsubsection}{Reporte}{372}{section*.537}%
\contentsline {subsubsection}{Imagen:}{372}{section*.538}%
\contentsline {subsubsection}{Transcripción:}{373}{section*.539}%
\contentsline {subsubsection}{Memo}{373}{section*.540}%
\contentsline {subsubsection}{Reporte}{373}{section*.541}%
\contentsline {subsubsection}{Imagen:}{373}{section*.542}%
\contentsline {subsubsection}{Transcripción:}{374}{section*.543}%
\contentsline {subsubsection}{Memo}{374}{section*.544}%
\contentsline {subsubsection}{Reporte}{374}{section*.545}%
\contentsline {subsubsection}{Imagen:}{374}{section*.546}%
\contentsline {subsubsection}{Transcripción:}{375}{section*.547}%
\contentsline {subsubsection}{Memo}{375}{section*.548}%
\contentsline {subsubsection}{Reporte}{375}{section*.549}%
\contentsline {subsubsection}{Imagen:}{375}{section*.550}%
\contentsline {subsubsection}{Transcripción:}{376}{section*.551}%
\contentsline {subsubsection}{Memo}{376}{section*.552}%
\contentsline {subsubsection}{Reporte}{376}{section*.553}%
\contentsline {subsubsection}{Imagen:}{376}{section*.554}%
\contentsline {subsubsection}{Transcripción:}{377}{section*.555}%
\contentsline {subsubsection}{Memo}{377}{section*.556}%
\contentsline {subsubsection}{Reporte}{377}{section*.557}%
\contentsline {subsubsection}{Imagen:}{377}{section*.558}%
\contentsline {subsubsection}{Transcripción:}{378}{section*.559}%
\contentsline {subsubsection}{Memo}{378}{section*.560}%
\contentsline {subsubsection}{Reporte}{378}{section*.561}%
\contentsline {subsubsection}{Imagen:}{378}{section*.562}%
\contentsline {subsubsection}{Transcripción:}{379}{section*.563}%
\contentsline {subsubsection}{Memo}{379}{section*.564}%
\contentsline {subsubsection}{Reporte}{379}{section*.565}%
\contentsline {subsubsection}{Imagen:}{379}{section*.566}%
\contentsline {subsubsection}{Transcripción:}{380}{section*.567}%
\contentsline {subsubsection}{Memo}{380}{section*.568}%
\contentsline {subsubsection}{Reporte}{380}{section*.569}%
\contentsline {subsubsection}{Imagen:}{380}{section*.570}%
\contentsline {subsubsection}{Transcripción:}{381}{section*.571}%
\contentsline {subsubsection}{Memo}{381}{section*.572}%
\contentsline {subsubsection}{Reporte}{381}{section*.573}%
\contentsline {subsubsection}{Imagen:}{381}{section*.574}%
\contentsline {subsubsection}{Transcripción:}{382}{section*.575}%
\contentsline {subsubsection}{Memo}{382}{section*.576}%
\contentsline {subsubsection}{Reporte}{382}{section*.577}%
\contentsline {subsubsection}{Imagen:}{382}{section*.578}%
\contentsline {subsubsection}{Transcripción:}{383}{section*.579}%
\contentsline {subsubsection}{Memo}{383}{section*.580}%
\contentsline {subsubsection}{Reporte}{383}{section*.581}%
\contentsline {subsubsection}{Imagen:}{383}{section*.582}%
\contentsline {subsubsection}{Transcripción:}{384}{section*.583}%
\contentsline {subsubsection}{Memo}{384}{section*.584}%
\contentsline {subsubsection}{Reporte}{384}{section*.585}%
\contentsline {subsubsection}{Imagen:}{384}{section*.586}%
\contentsline {subsubsection}{Transcripción:}{385}{section*.587}%
\contentsline {subsubsection}{Memo}{385}{section*.588}%
\contentsline {subsubsection}{Reporte}{385}{section*.589}%
\contentsline {subsubsection}{Imagen:}{385}{section*.590}%
\contentsline {subsubsection}{Transcripción:}{386}{section*.591}%
\contentsline {subsubsection}{Memo}{386}{section*.592}%
\contentsline {subsubsection}{Reporte}{386}{section*.593}%
\contentsline {subsubsection}{Imagen:}{386}{section*.594}%
\contentsline {subsubsection}{Transcripción:}{387}{section*.595}%
\contentsline {subsubsection}{Memo}{387}{section*.596}%
\contentsline {subsubsection}{Reporte}{387}{section*.597}%
\contentsline {subsubsection}{Imagen:}{387}{section*.598}%
\contentsline {subsubsection}{Transcripción:}{388}{section*.599}%
\contentsline {subsubsection}{Memo}{388}{section*.600}%
\contentsline {subsubsection}{Reporte}{388}{section*.601}%
\contentsline {subsubsection}{Imagen:}{388}{section*.602}%
\contentsline {subsubsection}{Transcripción:}{389}{section*.603}%
\contentsline {subsubsection}{Memo}{389}{section*.604}%
\contentsline {chapter}{\numberline {E}Anexo KICAD}{391}{appendix.Alph5}%
\contentsline {chapter}{\numberline {F}Anexo SATURAR}{397}{appendix.Alph6}%
