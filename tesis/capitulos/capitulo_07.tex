\chapter{Conclusiones}

A lo largo de los 6 capítulos que representan esta investigación, hemos propuesto pensar la enseñanza de programación con robótica educativa desde la perspectiva de las teorías del constructivismo socio – históricas, concretamente tomar el trabajo de Radford con la teoría de la objetivación. Esta postura nos llevó a tratar de mirar los procesos de aprendizaje y enseñanza de programación en espacios formales y no formales de aprendizaje, como las bibliotecas populares y las escuelas secundarias públicas para tratar de comprender las dinámicas de trabajo de estas comunidades desde la óptica de la T.O. 

Entender los procesos de enseñanza y aprendizaje en programación desde esta perspectiva teórica, nos impone algunas decisiones epistemológicas necesarias para poder dar cuenta del proceso de investigación que llevamos a cabo. Por eso, nuestra postura epistemológica con respecto a la construcción de “verdades” científicas dista de posicionarnos de una mirada positivista del conocimiento, entendiendo que es una quimera reflexionar en términos de verdades universales que puedan ser generalizadas.  El sistema educativo es un medio ambiente extremadamente complejo, al que solo podemos acceder a una porción minúscula de información con respecto a su inmensidad y complejidad, sumado al hecho de la imposibilidad de aislar variables de análisis, como haría por ejemplo un físico de partículas, o la capacidad de replicar nuestros “experimentos”. Entender esta complejidad, nos obligó a tener una honestidad epistemológica con respecto a las conclusiones que arribamos con nuestra investigación.

En principio decimos que la solidez de nuestras conclusiones se fundamentan en tomar las discusiones teóricas de las tradiciones del constructivismo socio – histórico para mirar nuestro problema de investigación, y usar estas discusiones para fundamentar nuestros argumentos.
Finalmente, podemos inferir que los resultados obtenidos nos permiten presentar algunas reflexiones de cierre relativas a las características que deberían tener la enseñanza y aprendizaje de la programación usando robots como herramienta desde de la perspectiva de la teoría de la objetivación de Radford. 
Desde esa perspectiva nuestras reflexiones finales las dividimos en: 

\begin{itemize}
	\item El carácter social de la actividad de programar
	\item La enseñanza como un quehacer político
	\item La no neutralidad de las tecnologías
	\item El software y hardware libre en las escuelas
\end{itemize}

\section{El carácter social de la actividad de programar}

Entendemos que el software, como producto cultural humano, es producido por personas que forma parte de comunidades, desde empresas, universidades hasta programadores que desarrollan software en su tiempo libre. Entender esta postura desde una perspectiva educativa, implica comprender que la actividad de programar, es siempre una labor conjunta entre el estudiante, el docente y las comunidades (muchas veces distribuidas en el mundo) con las que se establece un diálogo. El estudiante se encuentra con una cultura que no creo, que lo objeta, y necesita utilizar artefactos históricoculturales para aprender \citep[p. 19]{radford_teoriobjetivacion_2023}, pero esta cultura no está solamente circunscripta al aula, es global y está distribuida. Programar computadoras como actividad es, por tanto, una actividad social, atravesada por condicionamientos culturales, históricos y sociales que lo condicionan más allá de lo puramente técnico. 

Cuando un estudiante se embarca en el camino de aprender un lenguaje de programación (o varios) entra en diálogo con actores distribuidos globalmente, no solo por la participación directa en foros o chats online, la misma lógica de uso del lenguaje forma parte de este diálogo histórico donde el estudiante está siendo objetado por un lenguaje que fue producido a lo largo del tiempo por múltiples actores. Este aprendizaje expansivo \citep{engestrom_learning_2015} 
demuestra la importancia de contar con comunidades que refuercen el proceso de aprendizaje de los estudiantes, a su vez participar activamente de estas comunidades, debería ser un norte a seguir por una propuesta curricular específica de enseñanza de programación.

La elección de Python como lenguaje de programación inicial, no solo responde a ventajas técnicas con respecto a la facilidad de su sintaxis, o su rendimiento y velocidad de procesamiento. Elegir Python también responde a pensar con respecto a las comunidades de desarrollo que existen y que dialogan entre sí, desde desarrolladores Backend para páginas webs, hasta científicos que usan librerías de procesamiento matemático y simulaciones. 

Decimos entonces que la elección de un lenguaje para enseñar en el aula, no solo debería responder a las posibilidades técnicas de desarrollo que implemente, además debería poder contar con comunidades (locales e internacionales) de desarrollo activas, que expandan las capacidades del lenguaje.

\section{La enseñanza como un quehacer político}

Para Paulo Freire la educación solamente puede ser entendida como práctica de la libertad, es decir, como una acción social tendiente a la realización del ser humano \citep{freire_educacion_2008}. Hacemos nuestro el discurso de Freire con relación a la educación como práctica de la libertad, que implica la negación del hombre abstracto, aislado, suelto, desligado del mundo \citep[p. 91]{vogliotti_glosario_2012}, donde un ejercicio educativo desligado de la realidad concreta de la comunidad nada más puede ser entendido como una práctica alienante.

Desde esta perspectiva, Radford retoma las discusiones sobre como re imaginar el aula de matemática desde una praxis emancipadora, que tenga en cuenta las necesidades históricas y sociales de los estudiantes y sus docentes. Radford dice:

\begin{center}
	\begin{minipage}{0.9\linewidth}
		\vspace{5pt}%margen superior de minipage
		{\small
Además de ser siempre histórico y cultural, el aprendizaje también implica una transformación de la persona. Al aprender, la persona adquiere nuevas posibilidades para satisfacer sus necesidades y posicionarse frente a los demás. El aprendizaje, en otras palabras, es parte de la experiencia educativa a través de la cual nos formamos como seres humanos. Esto requiere conceptualizar el aprendizaje y la educación en general más allá de su característica instrumental frecuentemente atribuida. (Radford, 2023b, p. 19)
		}
		\begin{flushright}
		\citep[p. 19]{radford_politica_2023}
		\end{flushright}
		\vspace{5pt}%margen inferior de la minipage
	\end{minipage}
\end{center}

Retomamos las propuestas de la teoría de la objetivación para re imaginar la enseñanza de programación en las aulas de las escuelas de Córdoba y pensarlas no solo como generadoras de fuerza de trabajo, finalidad que entendemos importante pero no única. Decimos que los estudiantes son individuos que están no solamente aprendiendo algo, también están convirtiéndose en 'alguien' \citep{radford_ensenanza-aprendizaje_2014,radford_aspectos_2021}, y es necesario poner en el tapete de la discusión que herramienta de software usaremos, no solamente desde una mirada utilitaria, sino entendiendo que las tecnologías digitales son espacios de construcción de ciudadanía, y como tal atraviesan la vida cotidiana de todos las personas. La escuela contemporánea es inherentemente política y con un marcado carácter económico \citep[p. 52]{radford_politica_2023}. Vemos que los contenidos disciplinarios promovidos precisamente con respecto a la enseñanza de programación suelen están subordinados a la vida económica, dejando de lado posibles discusiones que puedan darse con respecto a la construcción de ciudadanía y la soberanía tecnológica. 

Entendemos que el acto de enseñar a programar requiere tener un compromiso ético y político con respecto a las tecnologías digitales, no solo pensar en la producción de fuerza de trabajo, o en saberes pedagógicos que puedan emerger de la práctica educativa.

\section{La no neutralidad de las tecnologías}

A lo largo de los capítulos que representan esta tesis doctoral, tratamos de dar cuenta de la necesidad de repensar las herramientas de tecnologías digitales para comprender el carácter ético y político que están implícitas en la elección de las mismas y su consiguiente implementación en el aula.

Elegir una herramienta tecnológica, no requiere solamente posicionarnos en aspectos técnicos aislados sobre alguna de sus posibilidades o carencias. Además, nos obliga a revisar las posibilidades éticas y políticas de su adquisición por parte del estado. ¿Es acaso hardware que permita ser reparado localmente?, ¿puede expandirse sin necesidad de contar con autorizaciones del fabricante?, ¿nos obliga a depender de un fabricante específico?

Cuando elegimos una herramienta digital para usar en el aula, debemos tener en cuenta esas preguntas y visibilizar las tensiones entre la tecnología y la soberanía de los estudiantes. Hacemos nuestra esta cita de Bordignon:

\begin{center}
	\begin{minipage}{0.9\linewidth}
		\vspace{5pt}%margen superior de minipage
		{\small
Un objetivo de todo país debe ser lograr un nivel adecuado de autonomía tecnológica, lo cual implica ejercer la capacidad de elección de aquello que se va a desarrollar y aquello que se importará. En este contexto no se debe confundir la soberanía tecnológica con la autosuficiencia a partir del desarrollo y fabricación de todo con tecnología nacional; por el contrario, se plantea un escenario de criterios propios de la tecnología que más conviene, donde ésta puede ser nacional o importada.
		}
		\begin{flushright}
			\citep[p. 81]{bordignon_soberanitecnologica_2015}
		\end{flushright}
		\vspace{5pt}%margen inferior de la minipage
	\end{minipage}
\end{center}

 Uno de los objetivos de esta tesis doctoral, fue demostrar la posibilidad de implementar una propuesta desde la perspectiva de la soberanía tecnológica, teniendo en cuenta la necesidad de pensar la tecnología educativa también en su carácter político. Freire menciona:
 
 \begin{center}
 	\begin{minipage}{0.9\linewidth}
 		\vspace{5pt}%margen superior de minipage
 		{\small
En ese sentido, el chileno Gabriel Bode, que hace más de dos años que trabaja con el método de la etapa postalfabetiación, contribuyó con un aporte de gran importancia. En su experiencia, observó que los campesinos sólo se interesaban por la discusión cuando la codificación se refería, directamente, a dimensiones concretas  de sus necesidades sentidas. Cualquier desvío en la codificación, así como cualquier intento del educador por orientar el diálogo, en la decodificación hacía otros rumbos que no fuesen los de sus necesidades sentidas provocaban su silencia y la indiferencia de aquellos.
 		}
 		\begin{flushright}
 			\citep[p. 145]{freire_pedagogioprimido._2015}
 		\end{flushright}
 		\vspace{5pt}%margen inferior de la minipage
 	\end{minipage}
 \end{center}

Disponer de tecnologías que permitan un cierto nivel de autonomía en su construcción y producción local, así como la posibilidad de estudiar, modificar, copiar y mejorar el mismo por parte de comunidades y empresas locales, que puedan resolver necesidades concretas de la comunidad involucrándose en el proceso, nos faculta a pensar en dimensiones concretas de la comunidad educativa, desde los intereses de los estudiantes, hasta las posibilidades del establecimiento a la hora de implementar estas propuestas. 

Vemos como a lo largo del proceso de investigación, situaciones de participación comunitaria y colectiva surgieron de manera espontánea cuando logramos involucrar a otros actores al proceso educativo. La producción local por parte de PYMES facilita la modificación y adecuación para el contexto así como permitir actores locales que funcionen como facilitadores de conocimiento para la escuela. Eliminar la caja negra con respecto al diseño de software y de hardware, nos permitió ahondar en los contenidos que los estudiantes pudieran acceder, por eso durante el cursado en la escuela IPEM 137, uno de los estudiantes se puso a modificar el firmware embebido a nivel de micropython para poder agregarle funcionalidades al proyecto, situación que con hardware y software privativo, no podría haber encarado.

\section{El software y hardware libre en las escuelas}

Por último, es parte de esta tesis doctoral demostrar la factibilidad de pensar la enseñanza de programación desde la perspectiva de las teorías de tradición constructivista socio histórica, tomando los aportes de Radford con respecto a la teoría de la objetivación así como el trabajo de Freire y la educación dialógica \citep{freire_pedagogioprimido._2015,freire_pedagogiesperanza:_2009,freire_educacion_2008}. Decimos que pensar la enseñanza de programación desde la perspectiva de la T.O. implica tener una posición ética y política sobre la elección de las soluciones tecnológicas que se busquen implementar en el aula.  Al contar con una licencia que permite copiar, estudiar, modificar y redistribuir código fuente, así como fomentar desarrollos locales de comunidades y PYMES, o cooperativas vemos como el software libre, y por consiguiente el hardware de especificaciones libres, puede ser considerado una herramienta idónea para su uso en el aula. 

El uso de software privativo en las escuelas, genera dependencia industrial por parte de un proveedor \citep{stallman_por_1999} que generalmente tendrá prácticas monopólicas con respecto a la defensa de sus intereses comerciales concretos, creando cajas negras sobre sus productos para que no puedan ser analizados o modificados.

Es común ver herramientas de tecnología educativa donde se cierran las posibilidades de modificación o reparación del hardware, aun hasta en casos donde se ha usado librerías y esquemáticos de proyectos libres como ARDUINO en su diseño. Entendemos que usar herramienta de software libre y hardware de especificaciones libres no es solo una elección económica donde prima la elección de una herramienta tecnológica solo por su coste asociado, o ventajas técnicas aparentes.
Hemos demostrado que es factible producir de forma local y autogestiva herramientas de tecnología educativa, capaces de ser fabricadas por los mismos actores de la comunidad escolar, en pequeña escala y con las mismas funcionalidades técnicas que otros productos comerciales.

Los sistemas de robótica educativa, permiten trabajar sobre contextos reales de aplicación, generando situaciones que afecten y modifiquen el medio ambiente de los estudiantes. Esta posibilidad demostró ser muy interesante para trabajar con estudiantes iniciales, que podían ver como los sistemas electrónicos accionaban sobre componentes físicos reales como luces del aula, o motores de ventiladores. Este efecto ayudó a mantener el interés de los estudiantes, dado que les permitía ver un producto concreto de su trabajo con Python. Algo parecido ocurrió cuando comenzamos a trabajar con chatbots computacionales basados en TELEGRAM. Sin embargo, también ocurrió que superado el efecto de interés por parte de los estudiantes, comenzaron a ver las posibilidades del hardware en términos más abstractos, ya no usando los dispositivos eléctricos, sino los indicadores LEDs de las placas, para luego directamente trabajar sobre una maqueta, por temas de comodidad y para poder tener un producto para mostrar en la muestra anual de fin de año de la escuela. Pasar de lo concreto (luces eléctricas por ejemplo) a lo abstracto (LED indicador de estado del hardware), pareciera una práctica habitual de los estudiantes cuando trabajan con hardware. La situación que tomó verdadera dimensión del proceso de labor conjunta entre docentes y estudiantes \citep{radford_teoriobjetivacion_2014} fue precisamente poder encarar la construcción del hardware entre los estudiantes y su profesora.

Poder trabajar directamente sobre la producción de hardware propicio la apropiación del proyecto por parte de la comunidad educativa, entendiendo que es un proyecto en el que pueden participar directamente en los procesos de diseño y desarrollo del hardware y el software, aunque eso requiera un compromiso superior a otras alternativas de tipo privativas, donde solo tienen acceso a un uso utilitario de la tecnolgía. Decimos que el software libre nos permite tener las siguientes ventajas \citep[p. 1]{cataldi_software_2008}:

\begin{itemize}
	\item Ventajas económicas: Nos permite reducir costos de licencias en toda la cadena de desarrollo del software, permitiendo además la participación de comunidades o PYMES.
	\item Ventajas legales: El uso de software privativo sin pagar sus respectivas licencias, es una práctica habitual en las escuelas, por motivos de desconocimiento con respecto a las implicaciones legales con respecto a la piratería, o simplemente por falta de dinero para su adquisición legal. Usar software libre permite a las instituciones mantenerse protegidas legalmente con respecto a posibles demandas gracias a las licencias libres.
	\item Estratégicos: El software libre se basa en la utilización de estándares abiertos para su desarrollo, esto garantiza que las aplicaciones puedan ser auditadas por motivos de seguridad (software malicioso), así como permitir el desarrollo de herramientas por parte de actores locales fortaleciendo el ecosistema de programadores nativos, permitiendo tener una perspectiva estratégica desde la soberanía nacional.
	\item Éticos: Entendemos que la escuela es un espacio de formación de ciudadanía, donde las implicaciones éticas y morales de las elecciones de las herramientas tecnológicas que se utilizaran en las aulas no pueden ser abarcadas solo desde un aspecto utilitario, las herramientas de software son espacios de discusión política, y en última instancia un terreno de disputa estratégica entre diversos actores como empresas, estados nacionales, comunidades y los mismos individuos que deberán usar esas herramientas. Es necesario poder involucrar a la comunidad escolar en las discusiones sobre el software.
\end{itemize}

La producción de hardware, aunque parezca una práctica novedosa, tiene una larga tradición de la mano de hobbistas, radio-aficionados y lo que hoy llamaríamos (empleando un anglicismo) como Makers \citep{ludena_educacion_2019,schrock_education_2014}. Revistas para niños y niñas como LUPIN, desde el año 1966 compartía con sus lectores los más variopintos planos de electrónica para fabricar de forma casera.

Estas prácticas, en la actualidad todavía comunes en escuelas técnicas, comenzaron un lento declive en la década de los noventa, con la aparición del 'usuario de pc' que vino a remplazar la enseñanza de programación por la práctica con herramientas digitales de ofimática, en las palabras de Wolovick y Martínez:

 \begin{center}
	\begin{minipage}{0.9\linewidth}
		\vspace{5pt}%margen superior de minipage
		{\small
En los ojos de un experto en computación, los 90s llegaron y una ola del neoliberalismo expresado en sus empresas monopólicas de software, desplazaron a la computadora programable por la computadora programada lista para usar. Se comenzaron a usar términos como Educación en Tecnologías de la Información, Ofimática, y TICs transversales. Las PCs originales y su aburrido aspecto, transformaron las horas de colores y sonidos de Logo en monótonas repeticiones de “clicks” en teclados y ratones dirigidos a productos de ofimática como procesadores de texto y planillas de cálculo. Se instaló la idea de que debíamos formar usuarios de tecnologías elaboradas por otros.
		}
		\begin{flushright}
			\citep[p. 9]{wolovick_ensenar_2016}
		\end{flushright}
		\vspace{5pt}%margen inferior de la minipage
	\end{minipage}
\end{center}

Entender la producción de hardware como parte de la dinámica escolar, nos permite agregar otra capa de complejidad a pensar la enseñanza de programación, no solo como productores de software, sino como productores de las herramientas físicas (o por lo menos, una parte de la misma) donde ese software trabajara.
La labor conjunta entre docente y estudiantes, se potencia gracias al trabajo con herramientas propias, que pueden ser rediseñadas en función de las necesidades concretas de la comunidad,  así como mantenidas y reparadas desde la misma institución.
 
El uso de software libre y hardware de especificaciones libres, permite eliminar las cajas negras que imposibilitan a los estudiantes, por lo menos a los que le interesen, ver las 'entrañas' de una herramienta digital, pudiendo entender de forma más precisa su funcionamiento y composición. Además, expande las posibilidades de la escuela, permitiendo interactuar con distintos actores que pueden participar en la producción de las herramientas de hardware, como escuelas técnicas o universidades que pueden aportar su conocimiento específico.

Apostar a comunidades de desarrollo dinámicas, abiertas y libres en términos de compartir código fuente, nos permite, en cierta manera, blindarnos al fenómeno QWERTY que Papert describió en su momento \citep{papert_desafio_1987}. Uno de los grandes problemas del lenguaje LOGO radico principalmente en la falta de comunidades que sostuvieran y formaran a docentes en el uso de la herramienta, así como la imposibilidad del lenguaje para volverse un sistema de propósito general más allá de su uso meramente educativo. Entendemos que la complejidad de trabajar con hardware implica una serie de compromisos que van desde la logística de producción y adquisición de los componentes, hasta la formación específica necesaria para poder llevar a cabo tamaña empresa. Desde nuestra perspectiva, es posible integrar a distintas instituciones del sector educativo, como universidades y terciarios, así como escuelas técnicas con orientación a la electrónica, para poder fabricar componentes que se usaran en el resto de las escuelas.

Experiencias que llevamos a cabo con la participación de estudiantes de la tecnicatura en mecatronica de la Universidad Tecnológica Nacional (UTN), donde fabricaron las placas domoticaro, así como un convenio con La Universidad Pedagógica Nacional (UNIPE) para fabricar placas y distribuir en escuelas de provincia de Buenos Aires, nos permiten pensar que es factible fabricar de manera local placas de electrónica diseñadas para su uso con escuelas secundarias, así como capacitar a docentes en su uso y posterior autoproducción con sus estudiantes.
Por último, pensar la enseñanza de programación en término de oficio, nos lleva a reflexionar una línea de trabajo futuro que tome como referencia la posibilidad de expandir este proceso de investigación para dar cuenta la actividad de programadores en formación profesional, para analizar sus interacciones cuando aprenden a programar un nuevo lenguaje, las estrategias que los estudiantes en formación profesional toman cuando aprenden, y como extrapolar ese saber a la enseñanza de programación en las escuelas secundarias.
